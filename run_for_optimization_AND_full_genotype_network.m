%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% To run the optimization schedule for parameter estimation for specific data and then get the full genotype network
% mentioned in the manuscript:
% Title: Synthetic Genotype Networks
% Authors: J Santos-Moreno, E Tasiudi,H Kusumawardhani1, J Stelling, Y Schaerli
% Date: August 30, 2022
% Author: Eve Tasiudi


% the additional packages have to added to path 
addpath(genpath('lib'));

addpath('.\src\models\Models_for_optimization');
addpath('.\src\common_scripts');
addpath('.\src\main_scripts_optimization');

% comment: the following commands are intensive and are suggested to be run on a server 
% Optimization 
run("mainOptimization.m")

%% Obtain full genotype network

addpath('.\src\main_scripts_full_genotype_network')
addpath('.\src\models\models_genotype')

run("genotypenetworks_cluster.m")

% and for control full genotype network run:
run("genotypenetworks_cluster_artificial_KD_Prom.m")

% for oscillating full genotype
run("genotypenetworks_oscillations")

% for control oscillating genotype
run("genotypenetworks_oscillations_artifial")


