%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% To create supplementary figures presented in the supplement of the manuscript:
% Title: Synthetic Genotype Networks
% Authors: J Santos-Moreno, E Tasiudi,H Kusumawardhani1, J Stelling, Y Schaerli
% Date: August 30, 2022
% Author: Eve Tasiudi

%% add neseccary directories
addpath('.\src\figure_generating_scripts\SharedScripts')
addpath(genpath('.\src\models'));
addpath('.\src\common_scripts')
addpath(genpath('.\lib'));
%% S2 %%
%for validating the mathematical models with the available experimental
%data, one has to run each simulation seperately depending on the number of
%edges the GRN has
% caseA are GRNs: 1.1-1.4
% caseB are GRNs: 2c.1-2c.8
% caseC are GRNs: 2b.1, 2b.2
% caseD are GRNs: 3.1 - 3.3
% caseE is GRN: 2a1
% caseF is GRN: 4.1
% example running 4.1 GRN:
addpath('.\src\figure_generating_scripts\FigS2_ModelSimulationsEstAndPred')
run("automate_Stripes.m")
rmpath('.\src\figure_generating_scripts\FigS2_ModelSimulationsEstAndPred')

%% S3 %%

% S3 upper row: Path length
% to obtain figure Path length starting from a Non Functional phenotype
addpath('.\src\figure_generating_scripts\FigS3_PathLengthAndCharFromOtherPhen')
run("plot_pathLength.m")
% to get path length starting from a Blue phenotype run same script but
% replace fnames = {'.\Data\ForFigures\PathCircuits_startingFromBlue.csv'};
% to get path length starting from a Green phenotype run same script but
% replace fnames = {'.\Data\ForFigures\PathCircuits_startingFromGreen.csv'};

% S3 middle row: positional robustness
% to obtain figure positional robustness of green
run("neutrality_script_supplement.m")

% for positional robustness in non-functional phenotype run script below
% by replacing data with the 
% readtable('.\data\ForFigures\neutrality_position_startingAtNF_Update_20k.csv'),
% similarly for green phenotype use:
% readtable('.\data\ForFigures\neutrality_position_startingAtGreen_Update_20k.csv');

% S3 bottom row: robustness
addpath('.\src\figure_generating_scripts\Fig5_MechanisticBasisRobAndEv')
% have to rerun for fig5 -evolvability to get correct clustering ID
run("crispri_graph_Fig5_Evolvability.m")

% robustness of non-functional phenotype 
run("crispri_graph_FigS3_RobustnessNF.m")

% robustness of blue phenotype 
run("crispri_graph_FigS3_RobustnessBlue.m")

rmpath('.\src\figure_generating_scripts\Fig5_MechanisticBasisRobAndEv')
rmpath('.\src\figure_generating_scripts\FigS3_PathLengthAndCharFromOtherPhen')

%% S4 %%

% S4.A %
% to generate this figure please use Rstudio and run the folder:
% Rscript_tree_grid

% S4.B/S4.C %
addpath('.\src\figure_generating_scripts\FigS4_GridAnalysis')
run("crispri_graph_FigSupGrid_ModelPredictedRobAndEv.m")

% S4.D %
% depending on the phenotype for the positional robustness select the data:
% non-functional: readtable('.\data\ForFigures\neutrality_position_startingAtNF_Update_20k-control.csv');
% blue: readtable('.\data\ForFigures\neutrality_position_startingAtBlue_Update_20k-control.csv');
% green: readtable('.\data\ForFigures\neutrality_position_startingAtGreen_Update_20k-control.csv');
 
run("neutrality_script_FigSupGrid.m")

% S4.F %
run("crispri_graph_FigSupGrid_Evolvability.m")

% S4.E %
run("crispri_graph_FigSupGrid_RobustnessNF.m")
run("crispri_graph_FigSupGrid_RobustnessBlue.m")
run("crispri_graph_FigSupGrid_RobustnessGreen.m")

rmpath('.\src\figure_generating_scripts\FigS4_GridAnalysis')

%% S5 %%

% S5.A and S5.B
addpath('.\src\figure_generating_scripts\FigS5_General_VF_growth_trajectories\growth')
run("growth_for_41.m")
run("growth_for2c4.m")
rmpath('.\src\figure_generating_scripts\FigS5_General_VF_growth_trajectories\growth')


% S5.C and S5.D
addpath('.\src\figure_generating_scripts\FigS5_General_VF_growth_trajectories\trajectories')
run("forward_simulate_model_trajectories.m")
rmpath('.\src\figure_generating_scripts\FigS5_General_VF_growth_trajectories\trajectories')

% S5.E
addpath('.\src\figure_generating_scripts\FigS5_General_VF_growth_trajectories\VF')
run("creating_excel_sh3.m")
rmpath('.\src\figure_generating_scripts\FigS5_General_VF_growth_trajectories\VF')


