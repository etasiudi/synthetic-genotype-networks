%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************

%% Main Figures

% add neseccary directories
addpath('.\src\figure_generating_scripts\sharedScripts')
addpath(genpath('.\src\models'));
addpath('.\src\common_scripts')
addpath(genpath('lib'));

%% Fig 3 %%

% time estimation to obtain Fig3B/Fig3C: ~1 sec
addpath('.\src\figure_generating_scripts\Fig3_ModelSimulationPrediction')

% Fig 3B and 3C %
run("mainFig3_prediction_stripes.m")

rmpath('.\src\figure_generating_scripts\Fig3_ModelSimulationPrediction')

%% Fig 4 %%
% time estimation to obtain Fig4: ~3 min
% Fig 4A %
% to generate this figure please use Rstudio and run the folder:
% scripts_for_R_tree_generation

addpath('.\src\figure_generating_scripts\Fig4_ModelPredictedRobAndEv')

% Fig 4B/4D
run("crispri_graph_Fig4_ModelPredictedRobAndEv.m")

% Fig 4C
% is done using Rstudio- please check folder: script_for_R_subnetwork

% Fig 4E
run("neutrality_script_Blue.m")

rmpath('.\src\figure_generating_scripts\Fig4_ModelPredictedRobAndEv')

%% Fig 5 %%
% time estimation to obtain Fig5: ~4 min
addpath('.\src\figure_generating_scripts\Fig5_MechanisticBasisRobAndEv')
% to ensure correct clustering between 5A, 5B: run sequentially

% Fig 5 A %
run("crispri_graph_Fig5_Evolvability.m")

% Fig 5 B %
run("crispri_graph_Fig5_RobustnessGreen.m")

rmpath('.\src\figure_generating_scripts\Fig5_MechanisticBasisRobAndEv')

%% Fig 6 %%
% time estimation to obtain Fig6: ~1 sec

addpath('.\src\figure_generating_scripts\Fig6_EpistaticInteractions')

run("plot_epistasis.m")

rmpath('.\src\figure_generating_scripts\Fig6_EpistaticInteractions')

