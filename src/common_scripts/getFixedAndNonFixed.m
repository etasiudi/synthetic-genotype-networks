%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [paramNameFixed, paramValueFixed, paramNameNotFixed, paramValueNotFixed,paramIdxNotFixed] = getFixedAndNonFixed(ParamTable)
% from the parameter.txt get the parameters that are fixed (fixed = 1), not
% fixed (fixed = 0) and promoter strength 
% if in the fixed value there is a number other than 0, multiply the value
% with the p0 value given

%these are to be sampled
[paramIdxNotFixed,~ ] = find(ParamTable.fixed == 0);
paramNameNotFixed = ParamTable.names(paramIdxNotFixed);
paramValueNotFixed = ParamTable.p0(paramIdxNotFixed);

% these are to be fixed, but multiply the param value with the
% corresponding value (1, 0.86, 0.1)x
[paramIdxFixed,~] = find(ParamTable.fixed ~= 0);
% 1 | ParamTable.fixed == 0.3 ...
%     |ParamTable.fixed == 0.86 | ParamTable.fixed == 0.4 | ParamTable.fixed == 0.5 | ParamTable.fixed == 0.7 | ParamTable.fixed == 0.1);

omultiply = @(x,y)(log10(((10.^x).*y)));
%ParamTable.p0(paramIdxFixed) = omultiply(ParamTable.p0(paramIdxFixed), ParamTable.fixed(paramIdxFixed));

paramNameFixed = ParamTable.names(paramIdxFixed);
paramValueFixed = omultiply(ParamTable.p0(paramIdxFixed), ParamTable.fixed(paramIdxFixed));

end
