%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [ICs, ICsError] = getInitialConditions(lumpParamNames,lumpParamValues,thisModel,GrowthParamData,growthFieldName,growthPosition,sgRNACasesValue,forPlot)

%% get position of Ara and growth parameters
[~,ARAInput] = ismember('ARA',lumpParamNames);

unlogLumpParamValues = 10.^lumpParamValues;

if strcmp(thisModel,'emptymodel')
    [r,~] = find(lumpParamValues == 0);
    unlogLumpParamValues(r) = 0;
end
unlogLumpParamValues(ARAInput) = 0;
% cells are grown overnight in LB
growthParametersAra0 = load('./src/growth_related_opt/Growth_in_LB/growth_parameters_inLB.mat');
unlogLumpParamValues(growthPosition) = growthParametersAra0.para1;


%% translation depending on node2/node3
[~,kMAPIAPosition] = ismember('kMAPIA',lumpParamNames);
if isempty(sgRNACasesValue)
    unlogLumpParamValues(kMAPIAPosition) = 2.6*14.57;
%     14.57; %for 3 node values 
    [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP','scmKate'},unlogLumpParamValues);

else
    unlogLumpParamValues(kMAPIAPosition) = 14.57; %for 2 node values
    [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP'},unlogLumpParamValues);
    
end    


options.method = 'stiff';
options.abstol = 1e-08;
options.reltol = 1e-08;
    try
         if isempty(sgRNACasesValue) && (strcmp(thisModel,'node3_caseA') || strcmp(thisModel,'node3_caseA_Bl_Ch') ||  strcmp(thisModel,'node3_caseA_Bm_Ch'))        
             ICstests = zeros(18,1);
             
         elseif isempty(sgRNACasesValue) && (strcmp(thisModel,'node3_caseB') || strcmp(thisModel,'node3_caseB_Bl_Ch') ||  strcmp(thisModel,'node3_caseB_Bh_Cl'))       
             ICstests = zeros(21,1);
             
         elseif isempty(sgRNACasesValue) && strcmp(thisModel,'node3_caseC') 
             ICstests = zeros(21,1);
             
          elseif isempty(sgRNACasesValue) && (strcmp(thisModel,'node3_caseD') || strcmp(thisModel,'node3_caseD_Bl_Ch'))
            ICstests = zeros(24,1);
            
         elseif isempty(sgRNACasesValue) && strcmp(thisModel,'node3_caseE') 
            ICstests = zeros(21,1);
            
         elseif isempty(sgRNACasesValue) && strcmp(thisModel,'node3_caseF') 
             ICstests = zeros(27,1);
             
         elseif isempty(sgRNACasesValue) && strcmp(thisModel,'emptymodel') 
             ICstests = zeros(27,1);
             
         else             
                     ICstests = zeros(9,1);
         end
% 360 
% paramSimValues(14) = 0;paramSimValues(15) = 0;paramSimValues(16) = 0;
% [0:1:360]
    tempICs = IQMPsimulate(sprintf('%s',thisModel),[0:1:360],ICstests,paramSimNames,paramSimValues,options);
%       save('tempICs.mat','tempICs');

    ICs = tempICs.statevalues(end,:) ; 
    ICsError = norm(tempICs.statevalues(end,:) - tempICs.statevalues(end-1,:));
    
    catch ME
        fprintf('cant find Steady States: %s\n', ME.message);
        ICs = zeros(length(tempICs.statevalues(end,:)),1);
        ICsError = 1e+08;
    end
        
% end


if forPlot
    figure()
for i = 1: (length(tempICs.states) + length(tempICs.variables))
subplot(5,5,i)
    if i<=length(tempICs.states)
        plot(tempICs.time,tempICs.statevalues(:,i))
        title(tempICs.states(i));
    else
        plot(tempICs.time,tempICs.variablevalues(:,i-length(tempICs.states)))
        title(tempICs.variables(i-length(tempICs.states)))
    end
end
end





% function dilutionValue = dilutionFunction(parameterValue)
%     
% % function for time derivative of individual time derivative
% ysdiff = @(para,t)((para(1)*para(2)*para(3)*exp(-para(3)*t))./(para(4)*(para(2)*exp(-para(3)*t) + 1).^(1/para(4) + 1)));
% 
% time = 30;
% dilutionValue = [ysdiff(parameterValue(2:5),time) + ysdiff(parameterValue(6:9),time) + ysdiff(parameterValue(10:13),time)];
% return

