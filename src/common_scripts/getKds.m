%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function KDs = getKds(ModelName,ParamSpecs)
constVal = [1 3 5 7 9 11];

if strcmp(ModelName,'node3_caseA')
    edgeNamesCaseA = [{'kfGCSG1','kbGCSG1','kfGBSG2','kbGBSG2','kfGCSG3','kbGCSG3'}]';
      for i = 1:3 %total number of edges
          [~,idx] = ismember(edgeNamesCaseA(constVal(i):constVal(i)+1),ParamSpecs.names);
          KDs(i) = (10.^ParamSpecs.p0(idx(2)))./(10.^ParamSpecs.p0(idx(1)));
          end
elseif strcmp(ModelName,'node3_caseB')
    edgeNamesCaseB = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGBSG4','kbGBSG4'}]';
      for i = 1:4 %total number of edges
          [~,idx] = ismember(edgeNamesCaseB(constVal(i):constVal(i)+1),ParamSpecs.names);
          KDs(i) = (10.^ParamSpecs.p0(idx(2)))./(10.^ParamSpecs.p0(idx(1)));
          end
elseif strcmp(ModelName,'node3_caseC')
    edgeNamesCaseC = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGASG4','kbGASG4'}]';
      for i = 1:4 %total number of edges
          [~,idx] = ismember(edgeNamesCaseC(constVal(i):constVal(i)+1),ParamSpecs.names);
          KDs(i) = (10.^ParamSpecs.p0(idx(2)))./(10.^ParamSpecs.p0(idx(1)));
          end
elseif strcmp(ModelName,'node3_caseD')
    edgeNamesCaseD = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGBSG4','kbGBSG4',...
                      'kfGASG5','kbGASG5'}]';
      for i = 1:5 %total number of edges
          [~,idx] = ismember(edgeNamesCaseD(constVal(i):constVal(i)+1),ParamSpecs.names);
          KDs(i) = (10.^ParamSpecs.p0(idx(2)))./(10.^ParamSpecs.p0(idx(1)));
          end
elseif strcmp(ModelName,'node3_caseE')
        edgeNamesCaseE = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGASG4','kbGASG4'}]';
      for i = 1:4 %total number of edges
          [~,idx] = ismember(edgeNamesCaseE(constVal(i):constVal(i)+1),ParamSpecs.names);
          KDs(i) = (10.^ParamSpecs.p0(idx(2)))./(10.^ParamSpecs.p0(idx(1)));
          end
elseif strcmp(ModelName,'node3_caseF')
        edgeNamesCaseF =  [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGBSG4','kbGBSG4',...
                      'kfGASG5','kbGASG5',...
                      'kfGASG6','kbGASG6'}]';
      for i = 1:6 %total number of edges
          [~,idx] = ismember(edgeNamesCaseF(constVal(i):constVal(i)+1),ParamSpecs.names);
          KDs(i) = (10.^ParamSpecs.p0(idx(2)))./(10.^ParamSpecs.p0(idx(1)));
      end
elseif strcmp(ModelName,'emptymodel')
      [whichPlaces,~] = find(contains(ParamSpecs.names,'kfG'));
      for i = 1:length(whichPlaces)
          KDs(i) = (10.^ParamSpecs.p0(whichPlaces(i)+1))./(10.^ParamSpecs.p0(whichPlaces(i)));
      end
      
end