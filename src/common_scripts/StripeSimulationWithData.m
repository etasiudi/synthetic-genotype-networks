
%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [ABC] = StripeSimulationWithData(ModelName,ParamSpecs,DAtaName,GrowthParam,TimeStripe)
% ,toSave
% disp('plotting min-max normalization')
timepointStripe = TimeStripe;
tspan = [0:1:1000];
[paramNameFixed, paramValueFixed, paramNameNotFixed, paramValueNotFixed,~] = getFixedAndNonFixed(ParamSpecs);

lumpParamNames = [paramNameFixed; paramNameNotFixed];
lumpParamValues = [paramValueFixed;paramValueNotFixed];
GrowthParamData = load(GrowthParam);
growthFieldName = fieldnames(GrowthParamData);
growthParamNames = sprintfc('para%d',[1:13]);
[~,growthPosition] = ismember(growthParamNames,lumpParamNames);

%% Obtain ICs (the parameter values should be in log)
forplot = false;
[ICs,ICsError] = getInitialConditions(lumpParamNames,lumpParamValues,ModelName,GrowthParamData,growthFieldName,growthPosition, [],forplot);

[~,ARAPosition] = ismember('ARA',lumpParamNames);
%test for maturation
[~,kMAPIAPosition] = ismember('kMAPIA',lumpParamNames);
possibleMAtvalues = [2*0.02, 2*0.02, 2*0.02, 2.2*0.02, 2.4*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.6*0.35, 2.6*0.94, 2.6*3, 2.6*6.56, 2.6*13.78, 2.6*14.57];
% [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.35, 0.94, 3, 6.56, 13.78, 14.57];


options.method = 'stiff';
options.abstol = 1e-08;
options.reltol = 1e-08;

if ICsError < 1e+08
    unlogLumpParamValues = 10.^lumpParamValues;
    arabinose = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];
%     ;0
    for i = 1:length(arabinose)
            try
                unlogLumpParamValues(ARAPosition) = arabinose(i);
                unlogLumpParamValues(growthPosition) = GrowthParamData.(growthFieldName{1})(i,:);
                % changing maturation values
                unlogLumpParamValues(kMAPIAPosition) = possibleMAtvalues(i);
                [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP','scmKate'},unlogLumpParamValues); 
                resultsSim(i) = IQMPsimulate(ModelName,tspan,ICs,paramSimNames,paramSimValues,options);
%                             save(sprintf('resultsSim_%d_%d.mat',(i),1),'resultsSim');

                catch ME
                fprintf('simulation without success: %s\n', ME.message);
                continue
            end
        end

end
%                             save(sprintf('resultsSim_%d_%d.mat',(i),1),'resultsSim');

Data = load(DAtaName);
idxTimeData = find(ismember(Data.timepoints,timepointStripe));
idxsimulation = find(ismember(tspan,timepointStripe));

% plot the stripes [16:-1:1];
% 
val = (([0.2;6.67E-02;2.22E-02;7.41E-03;2.47E-03;8.23E-04;2.74E-04;9.14E-05;3.05E-05;1.02E-05;3.39E-06;1.13E-06;3.76E-07;1.25E-07;4.18E-08;0]));


% 

% log(flip([2e-01; 6.67e-02; 2.22e-02; 7.41e-03; 2.47e-03; 8.23e-04; 2.74e-04; 9.14e-05; 3.05e-05; 1.02e-05; 3.39e-06; 1.13e-06; 3.76e-07; 1.25e-07; 4.18e-08; 0]));
%  
[~,nodePosition] = ismember({'Pa','Pb','Pc'},resultsSim(1).states');
% getPosition(resultsSim(1).states',{'Pa','Pb','Pc'});
araChar = sprintfc('ara%d',1:16);

%% considering data/OD

    for nAra = 1:16
        %data
        FluoSTD = getStDevFunction(DAtaName,Data,timepointStripe,araChar(nAra),[]);
        nodeAData(nAra) = Data.mKO2.(sprintf('ara%d',nAra))(idxTimeData)./Data.OD.(sprintf('ara%d',nAra))(idxTimeData);
        nodeBData(nAra) = Data.mKate2.(sprintf('ara%d',nAra))(idxTimeData)./Data.OD.(sprintf('ara%d',nAra))(idxTimeData);
        nodeCData(nAra) = Data.GFP.(sprintf('ara%d',nAra))(idxTimeData)./Data.OD.(sprintf('ara%d',nAra))(idxTimeData);
		
        Adatastd(nAra) = error_prop_division((nodeAData(nAra )),Data.mKO2.(sprintf('ara%d',nAra))(idxTimeData),...
        Data.OD.(sprintf('ara%d',nAra))(idxTimeData), FluoSTD(1),Data.ODstd.(sprintf('ara%d',nAra))(idxTimeData));
        
        Bdatastd(nAra) = error_prop_division((nodeBData(nAra )),Data.mKate2.(sprintf('ara%d',nAra))(idxTimeData),...
        Data.OD.(sprintf('ara%d',nAra))(idxTimeData), FluoSTD(2),Data.ODstd.(sprintf('ara%d',nAra))(idxTimeData));
    
        Cdatastd(nAra) = error_prop_division((nodeCData(nAra )),Data.GFP.(sprintf('ara%d',nAra))(idxTimeData),...
        Data.OD.(sprintf('ara%d',nAra))(idxTimeData), FluoSTD(3),Data.ODstd.(sprintf('ara%d',nAra))(idxTimeData));
        
        if ~isempty(resultsSim(nAra).time)
        %simulated
        
        nodeA(nAra) = resultsSim(nAra).statevalues(idxsimulation,nodePosition(1));
        nodeB(nAra) = resultsSim(nAra).statevalues(idxsimulation,nodePosition(2));
        nodeC(nAra) = resultsSim(nAra).statevalues(idxsimulation,nodePosition(3));
      
        else
            continue
        end

    end



jt = 1;
    figure(50+jt)
    hold on
    % the fig is created from right to left 
    plot(val(:),((nodeA./max(nodeA))),'--','color',[251/255 176/255 64/255],'MarkerSize', 12)
    plot(val(:),((nodeB./max(nodeB))),'--','color',[0/255 174/255 239/255],'MarkerSize', 12)
    plot(val(:),((nodeC./max(nodeC))),'--','color',[7/255 146/255 71/255],'MarkerSize', 12)
% below is for min-max normalization: not used in figures, just for
% observation
%     A = (nodeA-min(nodeA))./(max(nodeA)-min(nodeA));
%     B = (nodeB-min(nodeB))./(max(nodeB)-min(nodeB));
%     C = (nodeC-min(nodeC))./(max(nodeC)-min(nodeC));
%     plot(val(:),A,'--','color',[251/255 176/255 64/255],'MarkerSize', 12)
%     plot(val(:),B,'--','color',[0/255 174/255 239/255],'MarkerSize', 12)
%     plot(val(:),C,'--','color',[7/255 146/255 71/255],'MarkerSize', 12)
    ABC = [(nodeA./max(nodeA))',(nodeB./max(nodeB))',(nodeC./max(nodeC))'];
%     ABC = [A',B',C'];

    [maxA,posMaxA] = max(nodeAData);
    [maxB,posMaxB] = max(nodeBData);
    [maxC,posMaxC] = max(nodeCData);
    stripenodeA = nodeAData/maxA;
    stripenodeASTD = error_prop_division(stripenodeA,nodeAData,maxA,Adatastd,Adatastd(posMaxA));

%     
    stripenodeB = nodeBData/maxB;
    stripenodeBSTD = error_prop_division(stripenodeB,nodeBData,maxB,Bdatastd,Bdatastd(posMaxB));

%     
    stripenodeC = nodeCData/maxC;
    stripenodeCSTD = error_prop_division(stripenodeC,nodeCData,maxC,Cdatastd,Cdatastd(posMaxC));

    errorbar(val(:),stripenodeA,stripenodeASTD,'.','color',[251/255 176/255 64/255],'MarkerSize', 12);
    errorbar(val(:),stripenodeB,stripenodeBSTD,'.','color',[0/255 174/255 239/255],'MarkerSize', 12);
    errorbar(val(:),stripenodeC,stripenodeCSTD,'.','color',[7/255 146/255 71/255],'MarkerSize', 12);

%     Aexp = (nodeAData-min(nodeAData))./(max(nodeAData)-min(nodeAData));
%     Bexp = (nodeBData-min(nodeBData))./(max(nodeBData)-min(nodeBData));
%     Cexp = (nodeCData-min(nodeCData))./(max(nodeCData)-min(nodeCData));
%     
%     errorbar(val(:),Aexp,stripenodeASTD,'.','color',[251/255 176/255 64/255],'MarkerSize', 12)
%     errorbar(val(:),Bexp,stripenodeBSTD,'.','color',[0/255 174/255 239/255],'MarkerSize', 12)
%     errorbar(val(:),Cexp,stripenodeCSTD,'.','color',[7/255 146/255 71/255],'MarkerSize', 12)
    idxName = strfind(DAtaName,'unormalized_data');
    set(gca, 'XScale', 'log')
    fortitlecaseName = DAtaName(idxName+17:end-4);
    xlabel('Ara (%)')
    ylabel('Norm. Fluorescence')
    title(sprintf('Circuit %s at %dmin',fortitlecaseName,timepointStripe))




%% plot the trajectories (it is always false)


forPlot = false;
    
        if forPlot
            for j = 1:length(resultsSim)
            figure(150+j)
                for ij = 1: (length(resultsSim(j).states) + length(resultsSim(j).variables))
                subplot(5,5,ij)
                        if ij<=length(resultsSim(j).states)
                            plot(resultsSim(j).time,resultsSim(j).statevalues(:,ij))
                            title(resultsSim(j).states(ij));
                        else
                            plot(resultsSim(j).time,resultsSim(j).variablevalues(:,ij-length(resultsSim(j).states)))
                            title(resultsSim(j).variables(ij-length(resultsSim(j).states)))
                        end
                end
            end
        end

 %just using experimental stdeviation       
        %     errorbar(val(:),nodeAData(jt,:),Adatastd,'*','color',[200/255 200/255 10/255],'MarkerSize', 12)
% %     ./max(nodeAData(jt,:))
%     errorbar(val(:),nodeBData(jt,:),Bdatastd,'*','color',[0 0.4470 0.7410],'MarkerSize', 12)
% %     ./max(nodeBData(jt,:))
%     errorbar(val(:),nodeCData(jt,:),Cdatastd,'*','color',[0 0.5 0],'MarkerSize', 12)
%     ./max(nodeCData(jt,:))