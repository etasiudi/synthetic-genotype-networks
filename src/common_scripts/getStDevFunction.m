%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function StDevfull = getStDevFunction(DataName, Data, timeExp,araChar,sgRNACasesValue)
%always in simulation Observed #1st column Pa, #2 Pb, #3 Pc
% timeExp is minutes
% identify the data to be used for cases
idxName = strfind(DataName,'unormalized_data');
caseName = DataName(idxName:end);
if ~isempty(sgRNACasesValue)
    StDevfull(:,1) = mKO2StDevCases(Data,caseName,timeExp,convertCharsToStrings(araChar),sgRNACasesValue);
    StDevfull(:,2) = GFPStDevCases(Data,caseName,timeExp,convertCharsToStrings(araChar),sgRNACasesValue);
else
StDevfull(:,1) = mKO2StDevCases(Data,caseName,timeExp,convertCharsToStrings(araChar),[]);
StDevfull(:,2) = mKate2StDevCases(Data,caseName,timeExp,convertCharsToStrings(araChar));
StDevfull(:,3) = GFPStDevCases(Data,caseName,timeExp,convertCharsToStrings(araChar),[]);
end
end

