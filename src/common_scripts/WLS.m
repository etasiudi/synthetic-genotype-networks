%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function result = WLS(DAta,Simulation,timepointExp,tspan,fluoStDev,araChar, sgRNACasesValue)
% i have added noise for conf intervals,meigo was ran wo
% find which simulated data to get
idxsimulation = find(ismember(tspan,timepointExp));

% find which data to get
idxTimeData = find(ismember(DAta.timepoints,timepointExp));

if isempty(sgRNACasesValue)
    resmKO2 = (Simulation(idxsimulation, 1) - DAta.mKO2.(convertCharsToStrings(araChar))(idxTimeData))./fluoStDev(:,1);

    resmKate2 = (Simulation(idxsimulation, 2) - DAta.mKate2.(convertCharsToStrings(araChar))(idxTimeData))./fluoStDev(:,2);

    resGFP = (Simulation(idxsimulation, 3) - DAta.GFP.(convertCharsToStrings(araChar))(idxTimeData))./fluoStDev(:,3);

    res = [resmKO2;resmKate2; resGFP];
else
    resmKO2 = (Simulation(idxsimulation, 1) - DAta.mKO2.(convertCharsToStrings(araChar))(idxTimeData, sgRNACasesValue))./fluoStDev(:,1);
    
    resGFP = (Simulation(idxsimulation, 2) - DAta.GFP.(convertCharsToStrings(araChar))(idxTimeData, sgRNACasesValue))./fluoStDev(:,2);
    
    res = [resmKO2;resGFP];
end


%            save(sprintf('WLS_%d.mat',1),'resmKO2','resGFP');
% res2 = sum([resmKO2,resmKate2, resGFP].^2)

result = (sum(res(:).^2));
%            save(sprintf('result%d.mat',1),'result');
