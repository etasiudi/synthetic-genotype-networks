%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function value = GFPStDevCases(DAta,CaseName,TimeExp,araChar,sgRNACasesValue)
%TimeExp is minutes
idxTimeData = find(ismember(DAta.timepoints,TimeExp));
switch CaseName
    case {'unormalized_data_2node.mat'}
        k = 1;
%         [constpart1GFP,~] = find(((DAta.GFP.(araChar)(:,sgRNACasesValue)))<3*10^4);
%         constStDGFP = zeros(length(constpart1GFP),1) + 8.3618e+03;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:,sgRNACasesValue)))>=3*10^4);
%         valuePRE = [constStDGFP;0.24*(DAta.GFP.(araChar)(otherGFP,sgRNACasesValue)) + 1015];
%         value = valuePRE(idxTimeData);
        [constpart1GFP,~] = find(((DAta.GFP.(araChar)(:,sgRNACasesValue)))<7*10^4);
        constStDGFP = zeros(length(constpart1GFP),1) + 10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:,sgRNACasesValue))>=7*10^4 & (DAta.GFP.(araChar)(:,sgRNACasesValue))<= 2.5e+05));
        FP2covmata = [constStDGFP;0.13*(DAta.GFP.(araChar)(otherGFP,sgRNACasesValue)) + 1015];
        [otherGFPa,~] = find((DAta.GFP.(araChar)(:,sgRNACasesValue))>=2.5*10^5);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_11.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<8*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*500;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 8*10^3  & ((DAta.GFP.(araChar)(:)))<=5e+04);
%         FP2covmata = [constStDGFP;k*(0.06*(DAta.GFP.(araChar)(otherGFP)) + 20)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>5e+04);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*3.0201e+03;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
%         
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_12.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<5*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*3664;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 5*10^3  & ((DAta.GFP.(araChar)(:)))<=3e+04);
%         FP2covmata = [constStDGFP;k*(0.4*(DAta.GFP.(araChar)(otherGFP)) + 1664)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>3e+04);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*13664;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_13.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<8*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*735;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 8*10^3  & ((DAta.GFP.(araChar)(:)))<=3e+04);
%         FP2covmata = [constStDGFP;k*(0.08*(DAta.GFP.(araChar)(otherGFP)) + 95)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>3e+04);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*2.4951e+03;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_14.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<5*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*1.05e+03;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 5*10^3  & ((DAta.GFP.(araChar)(:)))<= 1.8e+04);
%         FP2covmata = [constStDGFP;k*(0.02*(DAta.GFP.(araChar)(otherGFP)) + 950)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>1.8e+04);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*1.3100e+03;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c1.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<2*10^4);
%         constStDGFP = zeros(length(constpart1),1) + k*765;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 2*10^4  & ((DAta.GFP.(araChar)(:)))<= 1e+05);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.sfGFP.(araChar)(otherGFP)) - 1235)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>1e+05);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*8765;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c2.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<3*10^4);
%         constStDGFP = zeros(length(constpart1),1) + k*1.7660e+03;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 3*10^4  & ((DAta.GFP.(araChar)(:)))<= 1e+05);
%         FP2covmata = [constStDGFP;k*(0.05*(DAta.GFP.(araChar)(otherGFP)) + 266)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>1e+05);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*5266;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

     case {'unormalized_data_2c3.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<5*10^4);
%         constStDGFP = zeros(length(constpart1),1) + k*4398;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 5*10^4  & ((DAta.GFP.(araChar)(:)))<= 10e+04);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.GFP.(araChar)(otherGFP)) + 1398)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>10e+04);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*7398;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c4.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<3.5*10^4);
%         constStDGFP = zeros(length(constpart1),1) + k*5120;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 3.5*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.0e+05);
%         FP2covmata = [constStDGFP;k*(0.01*(DAta.GFP.(araChar)(otherGFP)) + 4770)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.0e+05);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*6770;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c8.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<3*10^4);
%         constStDGFP = zeros(length(constpart1),1) + k*2.1550e+03;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 3*10^4  & ((DAta.GFP.(araChar)(:)))<= 1e+05);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.GFP.(araChar)(otherGFP)) - 845)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>1.0e+05);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*9155;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_33.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<0.5*10^5);
%         constStDGFP = zeros(length(constpart1),1) + k*492;
%         [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 0.5*10^5  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
%         FP2covmata = [constStDGFP;k*(0.04*(DAta.GFP.(araChar)(otherGFP)) - 1508)];
%         [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*8492;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2a1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2b1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2b2.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c5.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c6.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c7.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_31.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_32.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_41.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        
        case {'unormalized_data_2cNF1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.GFP.(araChar)(:)))<7*10^4);
        constStDGFP = zeros(length(constpart1),1) + k*10350;
        [otherGFP,~] = find(((DAta.GFP.(araChar)(:)))>= 7*10^4  & ((DAta.GFP.(araChar)(:)))<= 2.5e+05);
        FP2covmata = [constStDGFP;k*(0.13*(DAta.GFP.(araChar)(otherGFP)) + 1250)];
        [otherGFPa,~] = find(((DAta.GFP.(araChar)(:)))>2.5e+05);
        constStDGFPb = zeros(length(otherGFPa),1) + k*3.3750e+04;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

end