%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [BinOscil,QualtOscPos,QualtOsc] = getOscillPattern(ABC)

counter = 3;
while counter >=1
    
    x = ABC(:,counter);
    [a,lag] = xcorr(x,'coeff');
    pos = find(lag>0);
    t2 = lag(pos);
    x2 = a(pos);
    [maxPeak, minPeak] = peakdet(x2,0.01);
    
    if size(minPeak,1) < 2
        BinOsciltemp(counter) = 0;
        val(counter) = -100;
        counter = counter - 1;     
                
    else
        BinOsciltemp(counter) = 1;
        val(counter) = maxPeak(2,2) - minPeak(1,2);
        counter = counter - 1;       
    end        
    
end

BinOscil = any(BinOsciltemp);
[QualtOsc,QualtOscPos] = max(val);

    