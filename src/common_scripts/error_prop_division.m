%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function stdC = error_prop_division(C,meanA,meanB,stdA,stdB)

fa = ((stdA)./meanA).^2;
fb = ((stdB)./meanB).^2;
stdC = (abs(C).*sqrt(fa+fb));