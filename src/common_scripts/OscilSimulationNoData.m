%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [BinOscil,QualtOscPos,QualtOsc,promEff] = OscilSimulationNoData(ModelName,ParamSpecs,Ara, type)

[paramNameFixed, paramValueFixed, paramNameNotFixed, paramValueNotFixed,~] = getFixedAndNonFixed(ParamSpecs);

lumpParamNames = [paramNameFixed; paramNameNotFixed];
lumpParamValues = [paramValueFixed;paramValueNotFixed];
[paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP','scmKate'},lumpParamValues);
[~,ARAPosition] = ismember('ARA',paramSimNames);
[~,kMAPIAPosition] = ismember('kMAPIA',paramSimNames);

%ODEsolver settings
options.method = 'stiff';
options.abstol = 1e-08;
options.reltol = 1e-08;

unlogLumpParamValues = 10.^paramSimValues;
if strcmp(ModelName,'emptymodelOscil')
    [r,~] = find(paramSimValues == 0);
    unlogLumpParamValues(r) = 0;
end

% initial conditions
unlogLumpParamValues(ARAPosition) = 0;
unlogLumpParamValues(kMAPIAPosition) = 2.6*14.57;
ICs = IQMPsimulate(ModelName, [0:1:360], [], paramSimNames, unlogLumpParamValues);

arabinose = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];
possibleMAtvalues = [2*0.02, 2*0.02, 2*0.02, 2.2*0.02, 2.4*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.6*0.35, 2.6*0.94, 2.6*3, 2.6*6.56, 2.6*13.78, 2.6*14.57];

% simulation based on Ara
idxAra = (Ara == arabinose);
unlogLumpParamValues(ARAPosition) = Ara;
unlogLumpParamValues(kMAPIAPosition) = possibleMAtvalues(idxAra);
t1 = [0:10:30000];
simulation = IQMPsimulate(ModelName,t1,ICs.statevalues(end,:), paramSimNames, unlogLumpParamValues);
[~,FluoPos] = ismember({'Pa','Pb','Pc'},simulation.states);
result =  simulation.statevalues(:,FluoPos); 
ratioResult = (result./max(result))*100;
% check oscillation
[BinOscil,QualtOscPos,QualtOsc] = getOscillPattern(ratioResult);
[~,pBidx] = ismember('kMB',ParamSpecs.names);
[~,pCidx] = ismember('kMC',ParamSpecs.names);
promEff = [ParamSpecs.fixed(pBidx),ParamSpecs.fixed(pCidx)];

if type

            figure()
            hold on
            % the fig is created from right to left
            plot(t1,ratioResult(:,1),'-','color',[200/255 200/255 10/255],'MarkerSize', 12)
            plot(t1,ratioResult(:,2),'-','color',[0 0.4470 0.7410],'MarkerSize', 12)
            hold on
            plot(t1,ratioResult(:,3),'-','color',[0 0.5 0],'MarkerSize', 12)
            title(round(QualtOsc,4))
        
end
