%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [cost,paraOpt] = runMeigo(paramSpecs,para0,funobjstr,maxeval,threshold)

[~, Abp, ~, paramValueNotFixed,paramIdxNotFixed] = getFixedAndNonFixed(paramSpecs);

npara = length(para0);
problem = [];
opts = [];
problem.f = funobjstr;
problem.x_L = paramSpecs.bmin(paramIdxNotFixed);
problem.x_U = paramSpecs.bmax(paramIdxNotFixed);
problem.vtr = threshold;
disp(threshold)
opts.tolc = 1e-3;
opts.iterprint = 1;
% opts.plot = 2;
opts.ndiverse = 100*npara;
opts.maxeval = maxeval;
opts.maxtime = 8000;
% opts.inter_save=1;
opts.local.solver  = 'fminsearch'; 
opts.local.iterprint = 1;
opts.local.n1  = 10;
opts.local.n2  = 10; 
opts.local.finish  = 0;
opts.local.bestx  = 1;
opts.local.tol    = 1;
% load('/home/etasiudi/ver8_CRISPRi_modeling/results_estimation/est_sg1_2and1t4_c14_2c4_changingDegradation_a.mat')
% 
% load('/home/etasiudi/ver8_CRISPRi_modeling/results_estimation/est_all_cases_changingONLYdmRNA_a.mat')

problem.x_0 =  paramValueNotFixed;
Results = MEIGO(problem,opts,'ESS');
paraOpt = [Results.xbest];   
funobj = str2func(funobjstr);
cost = funobj(paraOpt);


