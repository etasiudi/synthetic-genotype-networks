%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [meigoParamValues, meigoParamNames, costlist] = setMeigo(paramValueNotFixed,paramSpecs, FlPlot, arabinoseExp, timepointExp,thisModel, thisData,GrowthParam, npoints, maxeval, sgRNACasesNames,jointOpt,node3Seq)

[paramNameFixed, paramValueFixed, paramNameNotFixed, paraestinit,paramIdxNotFixed] = getFixedAndNonFixed(paramSpecs);
meigoParamNames = paramNameNotFixed;

% bmin = paramSpecs.bmin(paramIdxNotFixed);
% bmax = paramSpecs.bmax(paramIdxNotFixed);
% 
% lhsmatrix = lhsdesign(npoints,length(paramSpecs.p0(paramIdxNotFixed)));
% paraestinit = zeros(npoints,length(paramSpecs.p0(paramIdxNotFixed)));
% meigoParamValues = zeros(length(paramSpecs.p0(paramIdxNotFixed)),npoints);
% costlist = zeros(npoints,1);
% 
% for ipoint = 1:npoints
%     for iparaest = 1:length(paramSpecs.p0(paramIdxNotFixed))
%             paraestinit(iparaest,ipoint) = lhsmatrix(ipoint,iparaest)*(bmax(iparaest) - bmin(iparaest)) + bmin(iparaest);
%     end
% end

lengthPara = length(paramNameNotFixed);
% -(8-length(sgRNACasesNames))*7; %8 is the total sgRNA constructs we can have

% initialization for cluster (prepare for parfor) TO BE DONE
FlPlot = false;
parfor ipoint = 1:npoints
    [~,ndata] = setObjectiveSimulation(paramValueNotFixed, paramSpecs, FlPlot, arabinoseExp, timepointExp...
    ,thisModel, thisData,GrowthParam,sgRNACasesNames,jointOpt,node3Seq);
    threshold = chi2inv(0.95,(ndata-lengthPara));%/2
    funobjstr = 'setObjectiveSimulation';
%     paraestinit(:,ipoint)
    [cost, alphaopt] = runMeigo(paramSpecs,paraestinit,funobjstr, maxeval,threshold);
    costlist(ipoint) = cost;
    meigoParamValues(:,ipoint) = alphaopt';
end