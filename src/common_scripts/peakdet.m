%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [maxval, minval]=peakdet(input, delta, perc)

maxval = [];
minval = [];

input = input(:);

if nargin < 3
    perc = (1:length(input))';
else
    perc = perc(:);
    if length(input)~= length(perc)
        error('Input vectors input and perc must have same length');
    end
end

if (length(delta(:)))>1
    error('Input argument DELTA must be a scalar');
end

if delta <= 0
    error('Input argument DELTA must be positive');
end

mn = Inf; mx = -Inf;
mnpos = NaN; mxpos = NaN;

lookformax = 1;

for i=1:length(input)
    this = input(i);
    if this > mx, mx = this; mxpos = perc(i); end
    if this < mn, mn = this; mnpos = perc(i); end

    if lookformax
        if this < mx-delta
            maxval = [maxval ; mxpos mx];
            mn = this; mnpos = perc(i);
            lookformax = 0;
        end
    else
        if this > mn+delta
            minval = [minval ; mnpos mn];
            mx = this; mxpos = perc(i);
            lookformax = 1;
        end
    end
end

end
