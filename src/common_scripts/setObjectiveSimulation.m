%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [objf, ndata] = setObjectiveSimulation(varargin)

%% Initialization

persistent paramSpecs paramNameNotFixed paramValueFixed paramNameFixed FlPlot arabinoseExp timepointExp thisModel thisData GrowthParam sgRNACasesNames lumpParamNames jointOpt node3Seq

paramValueNotFixed = varargin{1};
checkColumnVector = iscolumn(paramValueNotFixed);
if checkColumnVector == 0
    paramValueNotFixed = paramValueNotFixed';
end

if nargin > 1
    paramSpecs = varargin{2};
    FlPlot = varargin{3}; 
    arabinoseExp = varargin{4};
    timepointExp = varargin{5};
    thisModel = varargin{6};
    thisData = varargin{7};
    GrowthParam = varargin{8}; 
    
    [paramNameFixed, paramValueFixed, paramNameNotFixed, ~, ~] = getFixedAndNonFixed(paramSpecs);
    lumpParamNames = [paramNameFixed; paramNameNotFixed];       % lump parameter names
    
    sgRNACasesNames = varargin{9};
    jointOpt = varargin{10};        %if there is a joint opt process for 2node/3node
    node3Seq = varargin{11};        % select the model edges
    
%     if (~(exist(thisModel)))                                    % remember to change when using cluster:mexa64
%       switch thisModel
%         case sprintf('%s',thisModel)
%             modelname = IQMmodel(sprintf('%s.txtbc',thisModel));
%             IQMmakeMEXmodel(modelname,sprintf('%s',thisModel));
% % %             modelnameAra = IQMmodel(sprintf('%s_Ara0.txtbc',thisModel));
%             IQMmakeMEXmodel(modelnameAra,sprintf('%s_Ara0',thisModel));
%         end 
%     end

end

%% Running objective for 2-node/ 3-node
lumpParamValues = [paramValueFixed;paramValueNotFixed];

if ~jointOpt
    % save('lumpParamValues.mat','lumpParamValues','lumpParamNames');
    if ~isempty(sgRNACasesNames) 
        costvector    = []; % cost values for all sgRNAs
        modelSgParameters = [{'bSG', 'bA','kfGCSG','kbGCSG','kMC'}]'; % the names the model identifies ,'knode2'
        modelCommonParameters = [{'kfSGCAS', 'kbSGCAS','kMAPIA','dmRNAnode2','kPIAPA','dPA','kMCPIC','kPICPC','dPC','VMAX','ARA','ARAN','Km'...
            'scmKO2node2','scGFPnode2','para1','para2','para3','para4','para5','para6','para7','para8','para9','para10','para11'...
            ,'para12','para13','CasConnode2'}]';

        possiblesgRNAs = [{'sg1','sg2','sg3','sg4','sg5','sg6','sg4t4','sg1t4'}]';
        [~,IdxCommon] = ismember(modelCommonParameters,lumpParamNames);
        for i = 1:length(sgRNACasesNames)
            sgspecific = [{sprintf('bSG%s',sgRNACasesNames{i}) , sprintf('bA%s',sgRNACasesNames{i}),...
                sprintf('kfGCSG%s',sgRNACasesNames{i}),sprintf('kbGCSG%s',sgRNACasesNames{i}),sprintf('kMC%s',sgRNACasesNames{i})}]';
            [~,IdxSg] = ismember(sgspecific,lumpParamNames);
    %         PtotSg = [IdxCommon; IdxSg]; 
            [~,sgRNACasesValue] = ismember(sgRNACasesNames{i},possiblesgRNAs);
            costvector =  [costvector objectiveSimulation([lumpParamValues(IdxCommon); lumpParamValues(IdxSg)], [lumpParamNames(IdxCommon); modelSgParameters], FlPlot, arabinoseExp, timepointExp...
        ,thisModel, thisData,GrowthParam, sgRNACasesValue)];
        end
        objf = sum(costvector);
        
    else
        
        objf = objectiveSimulation(lumpParamValues, lumpParamNames, FlPlot, arabinoseExp, timepointExp...
        ,thisModel, thisData,GrowthParam,[]);

    end
    if ~isempty(sgRNACasesNames) 
        ndata = 2*length(sgRNACasesNames)*length(timepointExp)*length(arabinoseExp); 
    else 
        ndata = 3*length(timepointExp)*length(arabinoseExp);
    end
    
    
    
else
    costvector    = []; % cost values for all sgRNAs
    for nModels = 1:length(thisModel)

        if ~isempty(sgRNACasesNames{nModels}) 

            %going on to the joint opt for 2node HaventAdded States
            modelSgParameters = [{'bSG', 'bA','kfGCSG','kbGCSG','kMC'}]'; % the names the model identifies 
            model2nodeCommonParameters = [{'kfSGCAS', 'kbSGCAS','kMAPIA','dmRNAnode2','kPIAPA','dPA','kMCPIC','kPICPC','dPC','VMAX','ARA','ARAN','Km'...
                ,'para1','para2','para3','para4','para5','para6','para7','para8','para9','para10','para11','para12','para13','CasConnode2'}]';
            possiblesgRNAs = [{'sg1','sg2','sg3','sg4','sg5','sg6','sg4t4','sg1t4'}]'; % important for correct data selection
            [~,IdxCommon] = ismember(model2nodeCommonParameters,lumpParamNames);
            [~,IdxScaling] = ismember([{'scmKO2node2','scGFPnode2'}],lumpParamNames);
            scaling = [{'scmKO2','scGFP'}]';
                for i = 1:length(sgRNACasesNames{nModels})
                    sgspecific = [{sprintf('bSG%s',sgRNACasesNames{nModels}{i}) , sprintf('bA%s',sgRNACasesNames{nModels}{i}),...
                        sprintf('kfGCSG%s',sgRNACasesNames{nModels}{i}),sprintf('kbGCSG%s',sgRNACasesNames{nModels}{i}),sprintf('kMC%s',sgRNACasesNames{nModels}{i})}]';
                    [~,IdxSg] = ismember(sgspecific,lumpParamNames);
            %         PtotSg = [IdxCommon; IdxSg]; 
                    [~,sgRNACasesValue] = ismember(sgRNACasesNames{nModels}{i},possiblesgRNAs);
                    costvector =  [costvector objectiveSimulation([lumpParamValues(IdxCommon); lumpParamValues(IdxSg); lumpParamValues(IdxScaling)],...
                        [lumpParamNames(IdxCommon); modelSgParameters; scaling], FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, sgRNACasesValue)];
                end
            
        elseif ~strcmp(thisModel{nModels},'node2')
            

            common2And3NodeParameters = [{'kfSGCAS', 'kbSGCAS','kMAPIA','kPIAPA','dPA','kMCPIC','kPICPC','dPC','VMAX','ARA','ARAN','Km'...
                ,'para1','para2','para3','para4','para5','para6','para7','para8','para9','para10','para11','para12','para13'}]';
%  this is when maturation is changing    common2And3NodeParameters = [{'kfSGCAS', 'kbSGCAS','kMAPIA','kPIAPA','dPA','kMCPIC','dPC','VMAX','ARA','ARAN','Km'...
%                 ,'para1','para2','para3','para4','para5','para6','para7','para8','para9','para10','para11','para12','para13'}]';
            [~,IdxCommon2And3NodeParameters] = ismember(common2And3NodeParameters,lumpParamNames);
            node3Specific = [{'bA','kMBPIB','kPIBPB','kMC','bSG1','bSG2'}];
            [~,IdxNode3Specific] = ismember(node3Specific,lumpParamNames);
% 			
			if strcmp(thisModel{nModels},'node3_caseA')
				[~,IdxCaseASpecific] = ismember([{'scmKO2caseA','scGFPcaseA','scmKatecaseA','dmRNAcaseA','CasConcaseA'}]',lumpParamNames);
				CaseA3node = [{'scmKO2','scGFP','scmKate','dmRNAcaseA','CasConcaseA'}]';
				IdxEdges = [];
				for ij = 1:length(node3Seq{nModels})
% 					%define the edges in the 3node model 
						edgeSpecific = [{sprintf('kfGCSG%s',node3Seq{nModels}{ij}),sprintf('kbGCSG%s',node3Seq{nModels}{ij})}];
					[~,IdxEdgesA] = ismember(edgeSpecific,lumpParamNames);
					IdxEdges = [IdxEdges; IdxEdgesA'];
	%                 end
				end
% 				% ACTUAL NAMES OF EDGES IN MODEL 3, AGAIN SEQUENCE IS IMPORTANT'bSG1','bSG2',
				  edgeNames3NodeCaseA = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3'}]';

              costvector = [costvector objectiveSimulation(...
                  [lumpParamValues(IdxCommon2And3NodeParameters); lumpParamValues(IdxNode3Specific); lumpParamValues(IdxEdges);lumpParamValues(IdxCaseASpecific)],...
                  [lumpParamNames(IdxCommon2And3NodeParameters); lumpParamNames(IdxNode3Specific); edgeNames3NodeCaseA; CaseA3node]...
                  ,FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, [])];
				
			elseif strcmp(thisModel{nModels},'node3_caseB')	
                [~,IdxCaseBSpecific] = ismember([{'scmKO2caseB','scGFPcaseB','scmKatecaseB','dmRNAcaseB','CasConcaseB'}]',lumpParamNames);
				CaseB3node = [{'scmKO2','scGFP','scmKate','dmRNAcaseB','CasConcaseB'}]';
				IdxEdges = [];
				for ij = 1:length(node3Seq{nModels})
% 					%define the edges in the 3node model 
						edgeSpecific = [{sprintf('kfGCSG%s',node3Seq{nModels}{ij}),sprintf('kbGCSG%s',node3Seq{nModels}{ij})}];
					[~,IdxEdgesA] = ismember(edgeSpecific,lumpParamNames);
					IdxEdges = [IdxEdges; IdxEdgesA'];
	%                 end
				end
% 				% ACTUAL NAMES OF EDGES IN MODEL 3, AGAIN SEQUENCE IS IMPORTANT'bSG1','bSG2',
				  edgeNames3NodeCaseB = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGBSG4','kbGBSG4'}]';

              costvector = [costvector objectiveSimulation(...
                  [lumpParamValues(IdxCommon2And3NodeParameters); lumpParamValues(IdxNode3Specific); lumpParamValues(IdxEdges);lumpParamValues(IdxCaseBSpecific)],...
                  [lumpParamNames(IdxCommon2And3NodeParameters); lumpParamNames(IdxNode3Specific); edgeNames3NodeCaseB; CaseB3node]...
                  ,FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, [])];
            
			elseif strcmp(thisModel{nModels},'node3_caseC')	
                [~,IdxCaseCSpecific] = ismember([{'scmKO2caseC','scGFPcaseC','scmKatecaseC','dmRNAcaseC','CasConcaseC'}]',lumpParamNames);
				CaseC3node = [{'scmKO2','scGFP','scmKate','dmRNAcaseC','CasConcaseC'}]';
				IdxEdges = [];
				for ij = 1:length(node3Seq{nModels})
% 					%define the edges in the 3node model 
						edgeSpecific = [{sprintf('kfGCSG%s',node3Seq{nModels}{ij}),sprintf('kbGCSG%s',node3Seq{nModels}{ij})}];
					[~,IdxEdgesA] = ismember(edgeSpecific,lumpParamNames);
					IdxEdges = [IdxEdges; IdxEdgesA'];
	%                 end
				end
% 				% ACTUAL NAMES OF EDGES IN MODEL 3, AGAIN SEQUENCE IS IMPORTANT'bSG1','bSG2',
				  edgeNames3NodeCaseC = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGASG4','kbGASG4'}]';

              costvector = [costvector objectiveSimulation(...
                  [lumpParamValues(IdxCommon2And3NodeParameters); lumpParamValues(IdxNode3Specific); lumpParamValues(IdxEdges);lumpParamValues(IdxCaseCSpecific)],...
                  [lumpParamNames(IdxCommon2And3NodeParameters); lumpParamNames(IdxNode3Specific); edgeNames3NodeCaseC; CaseC3node]...
                  ,FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, [])];
            
            
			elseif strcmp(thisModel{nModels},'node3_caseD')	
                [~,IdxCaseDSpecific] = ismember([{'scmKO2caseD','scGFPcaseD','scmKatecaseD','dmRNAcaseD','CasConcaseD'}]',lumpParamNames);
				CaseD3node = [{'scmKO2','scGFP','scmKate','dmRNAcaseD','CasConcaseD'}]';
				IdxEdges = [];
				for ij = 1:length(node3Seq{nModels})
% 					%define the edges in the 3node model 
						edgeSpecific = [{sprintf('kfGCSG%s',node3Seq{nModels}{ij}),sprintf('kbGCSG%s',node3Seq{nModels}{ij})}];
					[~,IdxEdgesA] = ismember(edgeSpecific,lumpParamNames);
					IdxEdges = [IdxEdges; IdxEdgesA'];
	%                 end
				end
% 				% ACTUAL NAMES OF EDGES IN MODEL 3, AGAIN SEQUENCE IS IMPORTANT'bSG1','bSG2',
				  edgeNames3NodeCaseD = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGBSG4','kbGBSG4',...
                      'kfGASG5','kbGASG5'}]';

              costvector = [costvector objectiveSimulation(...
                  [lumpParamValues(IdxCommon2And3NodeParameters); lumpParamValues(IdxNode3Specific); lumpParamValues(IdxEdges);lumpParamValues(IdxCaseDSpecific)],...
                  [lumpParamNames(IdxCommon2And3NodeParameters); lumpParamNames(IdxNode3Specific); edgeNames3NodeCaseD; CaseD3node]...
                  ,FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, [])];
            
			elseif strcmp(thisModel{nModels},'node3_caseE')	
                [~,IdxCaseESpecific] = ismember([{'scmKO2caseE','scGFPcaseE','scmKatecaseE','dmRNAcaseE','CasConcaseE'}]',lumpParamNames);
				CaseE3node = [{'scmKO2','scGFP','scmKate','dmRNAcaseE','CasConcaseE'}]';
				IdxEdges = [];
				for ij = 1:length(node3Seq{nModels})
% 					%define the edges in the 3node model 
						edgeSpecific = [{sprintf('kfGCSG%s',node3Seq{nModels}{ij}),sprintf('kbGCSG%s',node3Seq{nModels}{ij})}];
					[~,IdxEdgesA] = ismember(edgeSpecific,lumpParamNames);
					IdxEdges = [IdxEdges; IdxEdgesA'];
	%                 end
				end
% 				% ACTUAL NAMES OF EDGES IN MODEL 3, AGAIN SEQUENCE IS IMPORTANT'bSG1','bSG2',
				  edgeNames3NodeCaseE = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGASG4','kbGASG4'}]';

              costvector = [costvector objectiveSimulation(...
                  [lumpParamValues(IdxCommon2And3NodeParameters); lumpParamValues(IdxNode3Specific); lumpParamValues(IdxEdges);lumpParamValues(IdxCaseESpecific)],...
                  [lumpParamNames(IdxCommon2And3NodeParameters); lumpParamNames(IdxNode3Specific); edgeNames3NodeCaseE; CaseE3node]...
                  ,FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, [])];
			
			elseif strcmp(thisModel{nModels},'node3_caseF')	
                [~,IdxCaseFSpecific] = ismember([{'scmKO2caseF','scGFPcaseF','scmKatecaseF','dmRNAcaseF','CasConcaseF'}]',lumpParamNames);
				CaseF3node = [{'scmKO2','scGFP','scmKate','dmRNAcaseF','CasConcaseF'}]';
				IdxEdges = [];
				for ij = 1:length(node3Seq{nModels})
% 					%define the edges in the 3node model 
						edgeSpecific = [{sprintf('kfGCSG%s',node3Seq{nModels}{ij}),sprintf('kbGCSG%s',node3Seq{nModels}{ij})}];
					[~,IdxEdgesA] = ismember(edgeSpecific,lumpParamNames);
					IdxEdges = [IdxEdges; IdxEdgesA'];
	%                 end
				end
% 				% ACTUAL NAMES OF EDGES IN MODEL 3, AGAIN SEQUENCE IS IMPORTANT'bSG1','bSG2',
				  edgeNames3NodeCaseF = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGBSG4','kbGBSG4',...
                      'kfGASG5','kbGASG5',...
                      'kfGASG6','kbGASG6'}]';

              costvector = [costvector objectiveSimulation(...
                  [lumpParamValues(IdxCommon2And3NodeParameters); lumpParamValues(IdxNode3Specific); lumpParamValues(IdxEdges);lumpParamValues(IdxCaseFSpecific)],...
                  [lumpParamNames(IdxCommon2And3NodeParameters); lumpParamNames(IdxNode3Specific); edgeNames3NodeCaseF; CaseF3node]...
                  ,FlPlot, arabinoseExp{:,nModels}, timepointExp{:,nModels}...
                ,thisModel{nModels}, thisData{nModels},GrowthParam{nModels}, [])];				
				
            end
        end
        
    end
    
    objf = sum(costvector);
    preNdata = [];
    for tNdata = 1:length(thisModel)
        if strcmp(thisModel{tNdata},'node2')
            preNdata = [preNdata 2*length(sgRNACasesNames{tNdata})*length(timepointExp{tNdata})*length(arabinoseExp{tNdata})];
        else
            preNdata = [preNdata 3*length(timepointExp{tNdata})*length(arabinoseExp{tNdata})];
        end
    end
    ndata = sum(preNdata);

%   edgeNames3Node
%   IdxEdges
%   lumpParamValues
%   lumpParamNames
end


    
        

        
