%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [objf] = objectiveSimulation(lumpParamValues,lumpParamNames, FlPlot, arabinoseExp, timepointExp,thisModel, thisData,GrowthParam, sgRNACasesValue)


GrowthParamData = load(GrowthParam);
Data = load(thisData);


nTimepoint = length(timepointExp);
objfcost = [];

tspan = [0:1:1000];

% find the position of constant input
[~,ARAPosition] = ismember('ARA',lumpParamNames);
growthParamNames = sprintfc('para%d',[1:13]);
[~,growthPosition] = ismember(growthParamNames,lumpParamNames);

%find position for translation
[~,kMAPIAPosition] = ismember('kMAPIA',lumpParamNames);

% to be used to position growth parameters depending on Ara
growthFieldName = fieldnames(GrowthParamData);

% find the position of the scaling factors
[~,scmKO2Position] = ismember('scmKO2',lumpParamNames);
[~,scGFPPosition] = ismember('scGFP',lumpParamNames);

if isempty(sgRNACasesValue)
    [~,scmKate2Position] = ismember('scmKate',lumpParamNames);
    [scalingPosition] = [scmKO2Position,scmKate2Position,scGFPPosition];
else 
    [scalingPosition] = [scmKO2Position,scGFPPosition];
end


%% Obtain ICs 
forplot = false;
[ICs,ICsError] = getInitialConditions(lumpParamNames,lumpParamValues,thisModel,GrowthParamData,growthFieldName,growthPosition, sgRNACasesValue,forplot);
% ICs

%ODEsolver settings
options.method = 'stiff';
options.abstol = 1e-08;
options.reltol = 1e-04;

if ICsError >= 1e+08
    objf = 1e+20;
    
else
    A = 0;
unlogLumpParamValues = 10.^lumpParamValues;

    if isempty(sgRNACasesValue)
        arabinose = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];
%         [2e-01; 6.67e-02; 2.22e-02; 7.41e-03; 2.47e-03; 8.23e-04; 2.74e-04; 9.14e-05; 3.05e-05; 1.02e-05; 3.39e-06; 1.13e-06; 3.76e-07; 1.25e-07; 4.18e-08; 0].*13.33./0.2;        
        araIdxData = [find(ismember(arabinose,arabinoseExp))]; 
        araChar = sprintfc('ara%d',araIdxData);
%                 possibleMAtvalues =  [2*0.02, 2*0.02, 2*0.02, 2.2*0.02, 2.4*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.6*0.35, 2.6*0.94, 2.6*3, 2.6*6.56, 2.6*13.78, 2.6*14.57];
% 
        possibleMAtvalues =  [2*0.02, 2*0.02, 2*0.02, 2.2*0.02, 2.4*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.6*0.35, 2.6*0.94, 2.6*3, 2.6*6.56, 2.6*13.78, 2.6*14.57];
%         10.^[-1.52 -1.3 -1.27 -1.42 -1.15 -1.01 -0.71 -0.34 0.23 0.62 1.17 1.59 2 2 1.87 2.42];

%          possibleMAtvalues = 
%           caseD and F: [0.03, 0.03, 0.03, 0.04, 0.06, 0.1, 0.3, 0.9, 2, 6, 15, 50, 100, 400, 700, 1000];
%          

    else
        arabinose = [13321.788,13.322,6.661,3.33,1.665,0.833,0.416,0.208,0];
%         [0.2 2.00e-04 1.00e-04 5.00e-05	2.50e-05 1.25e-05 6.25e-06 3.13e-06 0 ].*13.33./0.2;
        % find the arabinose experiments to be estimated
%         tol = 1e-05;ismembertol
        araIdxData = [find(ismember(arabinose,arabinoseExp))]; 
        araChar = sprintfc('ara%d',araIdxData);
        possibleMAtvalues = [1.2*0.02, 1.2*0.02, 1.2*0.02, 1*0.02, 1*0.02, 1*0.02, 1*0.02, 1*0.35, 1*14.57];
%         10.^[-1.52 -1.52 -1.66 -1.79 -1.78 -1.5 -1.18 -0.88 0.9];
%         [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.35, 14.57];
%         10.^[-1.52 -1.52 -1.66 -1.79 -1.78 -1.5 -1.18 -0.88 0.9];
%         
    end
ranNumber = randi(250,1);
        for a = 1: length(araIdxData)
        try
            
           % replace the constant inputs with the corresponding correct ones
           %it replaces the unlogged paraX with nonlog values
            unlogLumpParamValues(ARAPosition) = arabinose(araIdxData(a));
            unlogLumpParamValues(kMAPIAPosition) = possibleMAtvalues(araIdxData(a));
            if isempty(sgRNACasesValue)
                unlogLumpParamValues(growthPosition) = GrowthParamData.(growthFieldName{1})(araIdxData(a),:);
            else 
                unlogLumpParamValues(growthPosition) = GrowthParamData.(growthFieldName{1})(1,:);
            end

           % prepare parameters for simulation
           % remove the ICs names/values
           if isempty(sgRNACasesValue)
            [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP','scmKate'},unlogLumpParamValues);
           
           else
            [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP'},unlogLumpParamValues);
           end
           
           %% to estimate ICs in simulation/ with removed steady state
%            ICs
%            paramSimNames
%            paramSimValues(14) = 0;paramSimValues(15) = 0;paramSimValues(16) = 0;
            resultsSim = IQMPsimulate(thisModel,tspan,ICs,paramSimNames,paramSimValues,options);
    %         replace model_test 'model_TEST' with thisModel
%             save(sprintf('resultsSim_%d_%d.mat',araIdxData(a),sgRNACasesValue),'resultsSim');
            
           % get Positions of FP proteins
            if isempty(sgRNACasesValue)
                [~,fluoPosition] = ismember({'Pa','Pb','Pc'},resultsSim.states);
            else
                [~,fluoPosition] = ismember({'Pa','Pc'},resultsSim.states);
            end
            [~,nCellsPosition] = ismember('N',resultsSim.variables);
            
           % multiply with scaling/ ncells if division TRUE/False change
           % multiplication
           simulationMeas = getSimMultiply(resultsSim,fluoPosition,nCellsPosition).*[unlogLumpParamValues(scalingPosition)]';

           % variance functions
           fluoStDev = getStDevFunction(thisData, Data, timepointExp, araChar(a),sgRNACasesValue);
            % Weighted Least Squares
           objfcost(a) = WLS(Data,simulationMeas,timepointExp,tspan,fluoStDev,araChar(a),sgRNACasesValue);

               
%            save(sprintf('simulationMeas_%d.mat',a),'simulationMeas');
%             save(sprintf('fluoStDev_%d.mat',a),'fluoStDev');
           % add figures if true
           if FlPlot
               SimulationPlots(Data, simulationMeas, fluoStDev, resultsSim, araChar(a), timepointExp, tspan, objfcost(a),sgRNACasesValue,araIdxData,A,ranNumber) ;  
               
           end
           
           if isempty(sgRNACasesValue)
                A = A + 3;
           else
               A = A + 2;
           end
            catch ME
            fprintf('simulation without success objSim: %s\n', ME.message);
            objfcost(a) = 1e20;
            continue
        end
    end

objf = sum(objfcost);    
end

