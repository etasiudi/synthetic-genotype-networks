%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function value = mKate2StDevCases(DAta,CaseName,TimeExp,araChar)
idxTimeData = find(ismember(DAta.timepoints,TimeExp));
switch CaseName
    case {'unormalized_data_11.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))<3*10^2);
%         constStDGFP = zeros(length(constpart1),1) + k*43.6;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=3*10^2);
%         valuePRE  = [constStDGFP;k*(0.13*(DAta.mKate2.(araChar)(otherGFP))+ 4.704)];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_12.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))<100);
%         constStDGFP = zeros(length(constpart1),1) + k*24;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=100);
%         valuePRE  = [constStDGFP;k*(0.04*(DAta.mKate2.(araChar)(otherGFP))+ 20)];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
   case {'unormalized_data_13.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))<400);
%         constStDGFP = zeros(length(constpart1),1) + k*72;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=400);
%         valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP))+ 60)]; 
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
   case {'unormalized_data_14.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))<8*10^2);
%         constStDGFP = zeros(length(constpart1),1) + k*12;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=8*10^2 & ((DAta.mKate2.(araChar)(:)))<=2100);
%         FP2covmata = [constStDGFP;k*(0.04*(DAta.mKate2.(araChar)(otherGFP)) - 20)];
%         [otherGFPa,~] = find(((DAta.mKate2.(araChar)(:)))>2100);
%         constStDGFPb = zeros(length(otherGFPa),1) + k*65;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        
   case {'unormalized_data_2c1.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 50);
%         constStDGFP = zeros(length(constpart1),1) + k*6;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=50);
%         valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 4)]; 
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_2c2.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 50);
%         constStDGFP = zeros(length(constpart1),1) + k*6;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=50);
%         valuePRE  = [constStDGFP;k*(0.04*(DAta.mKate2.(araChar)(otherGFP)) + 4)]; 
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_2c3.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 100);
%         constStDGFP = zeros(length(constpart1),1) + k*9;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=100);
%         valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 6)];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_2c4.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 50);
%         constStDGFP = zeros(length(constpart1),1) + k*9;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=50);
%         valuePRE  = [constStDGFP;k*(0.1*(DAta.mKate2.(araChar)(otherGFP))) + 4]; 
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_2c8.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 100);
%         constStDGFP = zeros(length(constpart1),1) + k*14;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=100);
%         valuePRE  = [constStDGFP;k*(0.04*(DAta.mKate2.(araChar)(otherGFP))) + 10];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_33.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 100);
%         constStDGFP = zeros(length(constpart1),1) + k*7;
%         [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=100);
%         valuePRE  = [constStDGFP;k*(0.06*(DAta.mKate2.(araChar)(otherGFP))) + 1];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        
        case {'unormalized_data_2a1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2b1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2b2.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c5.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c6.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c7.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_31.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_32.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_41.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
        
        case {'unormalized_data_2cNF1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKate2.(araChar)(:)))< 500);
        constStDGFP = zeros(length(constpart1),1) + k*43;
        [otherGFP,~] = find(((DAta.mKate2.(araChar)(:)))>=500);
        valuePRE  = [constStDGFP;k*(0.03*(DAta.mKate2.(araChar)(otherGFP)) + 28)];
        value = valuePRE(idxTimeData);
end