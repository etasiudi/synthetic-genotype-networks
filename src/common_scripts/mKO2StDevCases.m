%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function value = mKO2StDevCases(DAta,CaseName,TimeExp,araChar,sgRNACasesValue)
idxTimeData = find(ismember(DAta.timepoints,TimeExp));
switch CaseName
    case {'unormalized_data_2node.mat'}
        k = 1;
        %the variance function from the 2node
%         [constpart1mKO2,~] = find(((DAta.mKO2.(araChar)(:,sgRNACasesValue)))<7000);
%         constStDmKO2 = zeros(length(constpart1mKO2),1) + 2152;
%         [othermKO2,~] = find(((DAta.mKO2.(araChar)(:,sgRNACasesValue)))>=7000);
%         valuePRE = [constStDmKO2;0.6*(DAta.mKO2.(araChar)(othermKO2,sgRNACasesValue))-2008];
%         value = valuePRE(idxTimeData);
        [constpart1mKO2,~] = find(((DAta.mKO2.(araChar)(:,sgRNACasesValue)))<6*10^3);
        constStDmKO2 = zeros(length(constpart1mKO2),1) + k*899;
        [othermKO2,~] = find(((DAta.mKO2.(araChar)(:,sgRNACasesValue)))>=7000 & ((DAta.mKO2.(araChar)(:,sgRNACasesValue)))<=2e+04);
        FP2covmata = [constStDmKO2;k*(0.08*(DAta.mKO2.(araChar)(othermKO2,sgRNACasesValue)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:,sgRNACasesValue)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_11.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<3*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*220;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>=3*10^3 & ((DAta.mKO2.(araChar)(:)))<=1e+04);
%         FP2covmata = [constStDGFP;k*(0.05*(DAta.mKO2.(araChar)(otherGFP)) + 70)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>1e+04);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*570;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
    case {'unormalized_data_12.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<1*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*243;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>=1*10^3 & ((DAta.mKO2.(araChar)(:)))<=6e+03);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.mKO2.(araChar)(otherGFP)) + 143)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>6e+03);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*743;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_13.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<2*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*150;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>=2*10^3 & ((DAta.mKO2.(araChar)(:)))<=1e+04);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.mKO2.(araChar)(otherGFP)) - 50)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>1e+04);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*950;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

     case {'unormalized_data_14.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<5*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*200;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>=5*10^3 & ((DAta.mKO2.(araChar)(:)))<=1.1e+04);
%         FP2covmata = [constStDGFP;k*(0.05*(DAta.mKO2.(araChar)(otherGFP)) - 50)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>1.1e+04);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*500;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

     case {'unormalized_data_2c1.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<1500);
%         constStDGFP = zeros(length(constpart1),1) + k*152;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 1500 & ((DAta.mKO2.(araChar)(:)))<=1e+04);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.mKO2.(araChar)(otherGFP)) + 2)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>1e+04);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*1002;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c2.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<1500);
%         constStDGFP = zeros(length(constpart1),1) + k*23;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 1500 & ((DAta.mKO2.(araChar)(:)))<=6000);
%         FP2covmata = [constStDGFP;k*(0.05*(DAta.mKO2.(araChar)(otherGFP)) - 52)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>6000);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*248;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c3.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<8*10^3);
%         constStDGFP = zeros(length(constpart1),1) + k*221;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 8*10^3 & ((DAta.mKO2.(araChar)(:)))<=1.5e+04);
%         FP2covmata = [constStDGFP;k*(0.02*(DAta.mKO2.(araChar)(otherGFP)) + 61)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>1.5e+04);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*361;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c4.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<1500);
%         constStDGFP = zeros(length(constpart1),1) + k*150;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 1500 & ((DAta.mKO2.(araChar)(:)))<=6000);
%         FP2covmata = [constStDGFP;k*(0.04*(DAta.mKO2.(araChar)(otherGFP)) + 94)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>6000);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*334;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

    case {'unormalized_data_2c8.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<1500);
%         constStDGFP = zeros(length(constpart1),1) + k*88;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 1500 & ((DAta.mKO2.(araChar)(:)))<=10000);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.mKO2.(araChar)(otherGFP)) - 137)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>10000);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*863;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

   case {'unormalized_data_33.mat'}
        k = 1;
%         [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<2000);
%         constStDGFP = zeros(length(constpart1),1) + k*69;
%         [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 2000 & ((DAta.mKO2.(araChar)(:)))<=6000);
%         FP2covmata = [constStDGFP;k*(0.1*(DAta.mKO2.(araChar)(otherGFP)) - 89)];
%         [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>6000);
%         constStDGFPb = zeros(length(otherGFPa),1)+ k*489;
%         valuePRE = [FP2covmata;constStDGFPb];
%         value = valuePRE(idxTimeData);
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        
        case {'unormalized_data_2a1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2b1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2b2.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c5.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c6.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_2c7.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_31.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_32.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        case {'unormalized_data_41.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);
        
        case {'unormalized_data_2cNF1.mat'}
        k = 1;
        [constpart1,~] = find(((DAta.mKO2.(araChar)(:)))<6*10^3);
        constStDGFP = zeros(length(constpart1),1) + k*899;
        [otherGFP,~] = find(((DAta.mKO2.(araChar)(:)))>= 6*10^3 & ((DAta.mKO2.(araChar)(:)))<=2e+04);
        FP2covmata = [constStDGFP;k*(0.08*(DAta.mKO2.(araChar)(otherGFP)) + 419)];
        [otherGFPa,~] = find(((DAta.mKO2.(araChar)(:)))>2e+04);
        constStDGFPb = zeros(length(otherGFPa),1)+ k*2.0189e+03;
        valuePRE = [FP2covmata;constStDGFPb];
        value = valuePRE(idxTimeData);

end