%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function paramForSimulation = getParameterTable(sgRNANamesUsed,PromEffic,ModelCase,ModelTopology)

%load the sgRNA specific parameters from txt
pathForsgRNAParameters = '.\\src\\parameters\\references\\';
parameterTables = cell(length(sgRNANamesUsed),1);
for i = 1:length(sgRNANamesUsed)
    parameterTables{i} = (readtable(sprintf([pathForsgRNAParameters '%s.txt'],sgRNANamesUsed{i})));
end

%load the reference parameters for each case
referenceParamSpecs = (readtable(sprintf([pathForsgRNAParameters 'reference_%s.txt'],ModelCase)));
if isempty(ModelTopology)
switch  ModelCase
    
    case {'node3_caseA'}
        %the first sgRNA
        [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = parameterTables{1}.p0([3:4]);
        %the second sgRNA
        [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = parameterTables{2}.p0([3:4]);
        %the third sgRNA
        [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg3) = parameterTables{3}.p0(3:4);
        % node B promoter efficiency
        [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
    case {'node3_caseB'}
        %the first sgRNA 'bSG1',1 
        [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = parameterTables{1}.p0([3:4]);
        %the second sgRNA 'bSG2', 1 
        [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = parameterTables{2}.p0([3:4]);
        %the third sgRNA
        [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg3) = parameterTables{3}.p0(3:4);
        %the forth sgRNA, it inhibits node B
        [~,positionsg4] = ismember({'kfGBSG4','kbGBSG4'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg4) = parameterTables{4}.p0(3:4);
        % node B promoter efficiency
        [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
    case {'node3_caseC'}
        %the first sgRNA 'bSG1',1 
        [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = parameterTables{1}.p0([3:4]);
        %the second sgRNA 'bSG2', 1 
        [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = parameterTables{2}.p0([3:4]);
        %the third sgRNA
        [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg3) = parameterTables{3}.p0(3:4);
        %the forth sgRNA, it inhibits node B
        [~,positionsg4] = ismember({'kfGASG4','kbGASG4'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg4) = parameterTables{4}.p0(3:4);
        % node B promoter efficiency
        [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
    case {'node3_caseD'}
        %the first sgRNA 'bSG1',1 
        [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = parameterTables{1}.p0([3:4]);
        %the second sgRNA 'bSG2', 1 
        [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = parameterTables{2}.p0([3:4]);
        %the third sgRNA
        [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg3) = parameterTables{3}.p0(3:4);
        %the forth sgRNA, it inhibits node B
        [~,positionsg4] = ismember({'kfGBSG4','kbGBSG4'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg4) = parameterTables{4}.p0(3:4);
        %the fifth sgRNA, it inhibits node A
        [~,positionsg5] = ismember({'kfGASG5','kbGASG5'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg5) = parameterTables{5}.p0(3:4);
        % node B promoter efficiency
        [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
      case {'node3_caseE'}
        %the first sgRNA 'bSG1',1 
        [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = parameterTables{1}.p0([3:4]);
        %the second sgRNA 'bSG2', 1 
        [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = parameterTables{2}.p0([3:4]);
        %the third sgRNA
        [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg3) = parameterTables{3}.p0(3:4);
        %the forth sgRNA, it inhibits node B
        [~,positionsg4] = ismember({'kfGASG4','kbGASG4'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg4) = parameterTables{4}.p0(3:4);
        % node B promoter efficiency
        [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
        
    case {'node3_caseF'}
        %the first sgRNA 'bSG1',1 
        [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = parameterTables{1}.p0([3:4]);
        %the second sgRNA 'bSG2', 1 
        [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = parameterTables{2}.p0([3:4]);
        %the third sgRNA
        [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg3) = parameterTables{3}.p0(3:4);
        %the forth sgRNA, it inhibits node B
        [~,positionsg4] = ismember({'kfGBSG4','kbGBSG4'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg4) = parameterTables{4}.p0(3:4);
        %the fifth sgRNA, it inhibits node A
        [~,positionsg5] = ismember({'kfGASG5','kbGASG5'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg5) = parameterTables{5}.p0(3:4);
        %the 6th sgRNA, it inhibits node A
        [~,positionsg6] = ismember({'kfGASG6','kbGASG6'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg6) = parameterTables{6}.p0(3:4);
        % node B promoter efficiency
        [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
end
else
    try 
    [~,idxComplex] = ismember({'kfSGCAS','kbSGCAS'},referenceParamSpecs.names);
    Nedges = length(sgRNANamesUsed);
    [~,positionEdgeSpecific] = ismember({'dmRNAcaseF','CasConcaseF'},referenceParamSpecs.names);
    if Nedges == 3
        referenceParamSpecs.p0(positionEdgeSpecific) = [0.86 3.16] ;
        
    elseif Nedges == 4
        referenceParamSpecs.p0(positionEdgeSpecific) = [0.80 3.34] ;
    
    elseif Nedges == 5
        referenceParamSpecs.p0(positionEdgeSpecific) = [2.49 4.00] ;
        
    elseif Nedges == 6
        referenceParamSpecs.p0(positionEdgeSpecific) = [2.40 3.8] ;
        
    end
    for m = 1:length(ModelTopology)
        if strcmp('Place1',ModelTopology(m))
            [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1','kfSGCASsg1','kbSGCASsg1'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg1) = [parameterTables{m}.p0([3:4]) referenceParamSpecs.p0(idxComplex)] ;
        
        elseif strcmp('Place2',ModelTopology(m))
            [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2','kfSGCASsg2','kbSGCASsg2'},referenceParamSpecs.names);
        referenceParamSpecs.p0(positionsg2) = [parameterTables{m}.p0([3:4]) referenceParamSpecs.p0(idxComplex)] ;
        
        elseif strcmp('Place3',ModelTopology(m))
            [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3','kfSGCASsg3','kbSGCASsg3'},referenceParamSpecs.names);
            referenceParamSpecs.p0(positionsg3) = [parameterTables{m}.p0(3:4) referenceParamSpecs.p0(idxComplex)] ;
            [~,PromEffsg3Pos] = ismember({'kMB3'},referenceParamSpecs.names);
            referenceParamSpecs.p0(PromEffsg3Pos) = 0.17;
            referenceParamSpecs.fixed(PromEffsg3Pos) = PromEffic(1);
            
        elseif strcmp('Place4',ModelTopology(m))
            [~,positionsg4] = ismember({'kfGBSG4','kbGBSG4','kfSGCASsg4','kbSGCASsg4'},referenceParamSpecs.names);
            referenceParamSpecs.p0(positionsg4) = [parameterTables{m}.p0(3:4) referenceParamSpecs.p0(idxComplex)] ;
            [~,PromEffsg4Pos] = ismember({'kMC4'},referenceParamSpecs.names);
            referenceParamSpecs.p0(PromEffsg4Pos) = 0.17;
            referenceParamSpecs.fixed(PromEffsg4Pos) = PromEffic(2);  
            
        elseif strcmp('Place5',ModelTopology(m))
            [~,positionsg5] = ismember({'kfGASG5','kbGASG5','kfSGCASsg5','kbSGCASsg5'},referenceParamSpecs.names);
            referenceParamSpecs.p0(positionsg5) = [parameterTables{m}.p0(3:4) referenceParamSpecs.p0(idxComplex)] ;
            [~,PromEffsg5Pos] = ismember({'kMC5'},referenceParamSpecs.names);
            referenceParamSpecs.p0(PromEffsg5Pos) = 0.17;
            referenceParamSpecs.fixed(PromEffsg5Pos) = PromEffic(2);
            
        elseif strcmp('Place6',ModelTopology(m))
            [~,positionsg6] = ismember({'kfGASG6','kbGASG6','kfSGCASsg6','kbSGCASsg6'},referenceParamSpecs.names);
            referenceParamSpecs.p0(positionsg6) = [parameterTables{m}.p0(3:4) referenceParamSpecs.p0(idxComplex)] ;
            [~,PromEffsg6Pos] = ismember({'kMB6'},referenceParamSpecs.names);
            referenceParamSpecs.p0(PromEffsg6Pos) = 0.17;
            referenceParamSpecs.fixed(PromEffsg6Pos) = PromEffic(1);
        end
    end
         [~,positionPromoterB] = ismember({'kMB'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterB) = PromEffic(1);
        % node C promoter efficiency
        [~,positionPromoterC] = ismember({'kMC'},referenceParamSpecs.names);
        referenceParamSpecs.fixed(positionPromoterC) = PromEffic(2);
       
        
     % remove the places that arent used (causes problems lated because 10^0 = 1)
     CandidatePlaces = {'Place1','Place2','Place3','Place4','Place5','Place6'};
     [~,idxCP] = find(~ismember(CandidatePlaces,ModelTopology));
     for CP = 1:length(idxCP)
        if strcmp('Place1',CandidatePlaces(idxCP(CP)))
            [~,positionsg1] = ismember({'kfGCSG1','kbGCSG1','kfSGCASsg1','kbSGCASsg1'},referenceParamSpecs.names);
        referenceParamSpecs(positionsg1,:) = [];
        
        elseif strcmp('Place2',CandidatePlaces(idxCP(CP)))
            [~,positionsg2] = ismember({'kfGBSG2','kbGBSG2','kfSGCASsg2','kbSGCASsg2'},referenceParamSpecs.names);
        referenceParamSpecs(positionsg2,:) = [];
        
        elseif strcmp('Place3',CandidatePlaces(idxCP(CP)))
            [~,positionsg3] = ismember({'kfGCSG3','kbGCSG3','kfSGCASsg3','kbSGCASsg3','kMB3'},referenceParamSpecs.names);
        referenceParamSpecs(positionsg3,:) = [];
            
        elseif strcmp('Place4',CandidatePlaces(idxCP(CP)))
            [~,positionsg4] = ismember({'kfGBSG4','kbGBSG4','kfSGCASsg4','kbSGCASsg4','kMC4'},referenceParamSpecs.names);
        referenceParamSpecs(positionsg4,:) = [];
        
        elseif strcmp('Place5',CandidatePlaces(idxCP(CP)))
            [~,positionsg5] = ismember({'kfGASG5','kbGASG5','kfSGCASsg5','kbSGCASsg5','kMC5'},referenceParamSpecs.names);
        referenceParamSpecs(positionsg5,:) = [];
        
        elseif strcmp('Place6',CandidatePlaces(idxCP(CP)))
            [~,positionsg6] = ismember({'kfGASG6','kbGASG6','kfSGCASsg6','kbSGCASsg6','kMB6'},referenceParamSpecs.names);
        referenceParamSpecs(positionsg6,:) = [];
        end
    end
     
    catch
        disp('number of sgRNAs and number of edges in model incompatible')
    end   
    
    %for when promoter efficiency if 0
    Nparameters = length(referenceParamSpecs.p0);
    if Nparameters~=nnz(referenceParamSpecs.fixed)
        [rNNZ,~] = find(referenceParamSpecs.fixed == 0);
        referenceParamSpecs.p0(rNNZ) = 0;
        
    end
        end


paramForSimulation = referenceParamSpecs;
        
