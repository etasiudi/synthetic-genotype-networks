%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% forward simulate model predictions with average growth data and the corresponding available data %%
%% GRNs in caseE: 2a.1
% when deciding on which model to run with the data
% the variables that can be changed are:
% thisData, GrowthParam, sequenceSgRNA (the inhibitions by specific sgRNAs
% in the correct order)

close all


[~, ~, ~, ~,paramIdxNotFixed] = getFixedAndNonFixed(paramSpecs);


% find the common parameters
caseBSpecific = {'kMAPIA';'kPIAPA';'dPA';'VMAX';'ARA';'ARAN';'Km';'kMCPIC';'dPC';'kfSGCAS';'kbSGCAS';...
    'kMC';'kMBPIB';'dPB';'bSG1';'bSG2';'bA';'CasConcaseE';'dmRNAcaseE';'kPICPC';'kPIBPB';...
    'scmKO2caseE';'scGFPcaseE';'scmKatecaseE';'para1';'para2';'para3';'para4';'para5';'para6';'para7';'para8';'para9';'para10';'para11';'para12';'para13'};
[~,findCommon] = ismember(caseBSpecific,paramSpecs.names);

% get the correct sgRNAs repression
for ij = 1:length(sequenceSgRNA)

    edgeSpecific{ij} = [{sprintf('kfGCSG%s',sequenceSgRNA{ij});sprintf('kbGCSG%s',sequenceSgRNA{ij})}];

end
edgeSpecific_A = vertcat(edgeSpecific{:}); 
[~,findCorrectSgRNAs] = ismember(edgeSpecific_A, paramSpecs.names);

idxInParamSpecs = [findCommon;findCorrectSgRNAs];

% what the actual names of sgRNAs are in the model
edgeNames3NodeCaseB = [{'kfGCSG1','kbGCSG1',...
						'kfGBSG2','kbGBSG2',...
					  'kfGCSG3','kbGCSG3',...
                      'kfGASG4','kbGASG4'}]';
                  
% append all correct names
names = [caseBSpecific;edgeNames3NodeCaseB];
bmin = paramSpecs.bmin(idxInParamSpecs);
bmax = paramSpecs.bmax(idxInParamSpecs);
fixed = paramSpecs.fixed(idxInParamSpecs);
p0 = paramSpecs.p0(idxInParamSpecs);
ParameterTable = table(names,bmin,bmax,fixed,p0)
ParameterTable.names{22} = 'scmKO2';
ParameterTable.names{23} = 'scGFP';
ParameterTable.names{24} = 'scmKate';
% ParameterTable.p0(18) = 3.38;
% ParameterTable.p0(19) = 0.74;
ABC = StripeSimulationWithData(thisModel,ParameterTable,thisData,GrowthParam,900)
[percent] = getStripeNodealt(flip(round(ABC,3)));

idxName = strfind(thisData,'unormalized_data');
fortitlecaseName = thisData(idxName+17:end-4);
h = gcf
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,sprintf('Circuit %s at 900min ',fortitlecaseName),'-dpdf','-r0')

