%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [percent,stripeIdx,avPRCside] = getStripeNodealt(ABC)

[~,c] = max(ABC);

Position = abs(c-median(c));

if length(nonzeros(Position))~= 2
    stripeIdx = 1;
    percent = 100;
else 
    [~, stripeIdx] = min(Position);
    positionsEval = min([abs(16-c(stripeIdx)) abs(1-c(stripeIdx))]);
    PercentChange = @(xp,xi,xnorm)((xp-xi)./xnorm).*100;
    
    if stripeIdx~= 1
        
    PercentChangeFromPoint = PercentChange(ABC(c(stripeIdx),stripeIdx),ABC(1:2,stripeIdx),ABC(c(stripeIdx),stripeIdx));
    PercentChangeFromPoint = [PercentChangeFromPoint; PercentChange(ABC(end - 1:end,stripeIdx),ABC(c(stripeIdx),stripeIdx),ABC(c(stripeIdx),stripeIdx))];
    Premean = abs(mean(PercentChangeFromPoint(1:2)));
    Aftermean = abs(mean(PercentChangeFromPoint(3:4)));

    avPRCside = min([Premean Aftermean]);
        if avPRCside <= 6.00000001
            percent = 300;
            stripeIdx = 1;
        else
            percent = abs(Aftermean/Premean);
            stripeIdx = stripeIdx;
        end


    else
    percent = 200;
    stripeIdx = 1;
    end
end

