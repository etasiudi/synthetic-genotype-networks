%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [percent,stripeIdx,KDs,promEff] = StripeSimulationNoData(ModelName,ParamSpecs,Ara,timepointStripe, type,number,GrowthParam)

[paramNameFixed, paramValueFixed, paramNameNotFixed, paramValueNotFixed,~] = getFixedAndNonFixed(ParamSpecs);

lumpParamNames = [paramNameFixed; paramNameNotFixed];
lumpParamValues = [paramValueFixed;paramValueNotFixed];

GrowthParamData = load(GrowthParam);
growthFieldName = fieldnames(GrowthParamData);
growthParamNames = sprintfc('para%d',[1:13]);
[~,growthPosition] = ismember(growthParamNames,lumpParamNames);

%% Obtain ICs (the parameter values should be in log)
forplot = false;
[ICs,ICsError] = getInitialConditions(lumpParamNames,lumpParamValues,ModelName,GrowthParamData,growthFieldName,growthPosition, [],forplot);

[~,ARAPosition] = ismember('ARA',lumpParamNames);
[~,kMAPIAPosition] = ismember('kMAPIA',lumpParamNames);

%ODEsolver settings
options.method = 'stiff';
options.abstol = 1e-08;
options.reltol = 1e-08;

% if ICsError < 1e+08

unlogLumpParamValues = 10.^lumpParamValues;
if strcmp(ModelName,'emptymodel')
    [r,~] = find(lumpParamValues == 0);
    unlogLumpParamValues(r) = 0;
end

    if Ara
        
        arabinose = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];
        possibleMAtvalues = [2*0.02, 2*0.02, 2*0.02, 2.2*0.02, 2.4*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.3*0.02, 2.6*0.35, 2.6*0.94, 2.6*3, 2.6*6.56, 2.6*13.78, 2.6*14.57];
        for i = 1:length(arabinose)
            try
                unlogLumpParamValues(ARAPosition) = arabinose(i);
                unlogLumpParamValues(growthPosition) = GrowthParamData.(growthFieldName{1})(i,:);
                unlogLumpParamValues(kMAPIAPosition) = possibleMAtvalues((i));

    [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP','scmKate'},unlogLumpParamValues);

                resultsSim(i) = IQMPsimulate(ModelName,[0:1:1000],ICs,paramSimNames,paramSimValues,options);
%                 save(sprintf('resultsSim_%d_%d.mat',i,i),'resultsSim');

                catch ME
                fprintf('simulation without success: %s\n', ME.message);
                continue
            end
        end
%                         save(sprintf('resultsSim_%d_%d.mat',i,i),'resultsSim');

        % plot the stripes
        val = [16:-1:1]; 
        times = find(ismember([0:1:1000],timepointStripe));
        [nodePosition,~] = find(ismember(resultsSim(1).states',{'Pa','Pb','Pc'}));

        for t = 1:length(times)
        for nAra = 1:16
            nodeA(t,nAra) = resultsSim(nAra).statevalues(times(t),nodePosition(1));
            nodeB(t,nAra) = resultsSim(nAra).statevalues(times(t),nodePosition(2));
            nodeC(t,nAra) = resultsSim(nAra).statevalues(times(t),nodePosition(3));
        end
        end
        A = nodeA./max(nodeA);
        B = nodeB./max(nodeB);
        C = nodeC./max(nodeC);
        ABC = [A',B',C'];
        [percent,stripeIdx] = getStripeNodealt(flip(round(ABC,3)));
        KDs = getKds(ModelName,ParamSpecs);
        [~,pBidx] = ismember('kMB',ParamSpecs.names);
        [~,pCidx] = ismember('kMC',ParamSpecs.names);
        promEff = [ParamSpecs.fixed(pBidx),ParamSpecs.fixed(pCidx)];
if type

        for jt = 1:length(times)
            figure()
%             subplot(3,3,number)
            hold on
            % the fig is created from right to left
            plot(val(:),nodeA(jt,:)./max(nodeA(jt,:)),'-','color',[200/255 200/255 10/255],'MarkerSize', 12)
            plot(val(:),nodeB(jt,:)./max(nodeB(jt,:)),'-','color',[0 0.4470 0.7410],'MarkerSize', 12)
            hold on
            plot(val(:),nodeC(jt,:)./max(nodeC(jt,:)),'-','color',[0 0.5 0],'MarkerSize', 12)
            title(round(percent,4))
        end
        
end
            
        

    else 
        arabinose = Ara;
            try
                unlogLumpParamValues(ARAPosition) = arabinose;
                [paramSimNames,paramSimValues] = getParamforSimulation(lumpParamNames,{'scmKO2','scGFP','scmKate'},unlogLumpParamValues);
                resultsSim = IQMPsimulate(ModelName,[0:10:1000],ICs,paramSimNames,paramSimValues,options);
                catch ME
                fprintf('simulation without success: %s\n', ME.message);
 
            end
    end
    
    % plot the trajectories (it is always false)
    forPlot = false;
    
        if forPlot
            for j = 1:length(resultsSim)
            figure(150+j)
                for ij = 1: (length(resultsSim(j).states) + length(resultsSim(j).variables))
                subplot(6,6,ij)
                        if ij<=length(resultsSim(j).states)
                            plot(resultsSim(j).time,resultsSim(j).statevalues(:,ij))
                            title(resultsSim(j).states(ij));
                        else
                            plot(resultsSim(j).time,resultsSim(j).variablevalues(:,ij-length(resultsSim(j).states)))
                            title(resultsSim(j).variables(ij-length(resultsSim(j).states)))
                        end
                end
            end
        end
        

        
        
    
% else
%     fprintf('Cant reach steady state for ICs, error too large.')
% end
