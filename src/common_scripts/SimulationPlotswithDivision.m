%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [] = SimulationPlotswithDivision(DAta, simulationMeas, fluoStDev, results, araChar, timepointExp, tspan, objfcost,sgRNACasesValue,araIdxData,A)

idxTimeData = find(ismember(DAta.timepoints,timepointExp));
idxsimulation = find(ismember(tspan,timepointExp));

% figure()
% for i = 1: (length(results.states) + length(results.variables))
% subplot(5,5,i)
%     if i<=length(results.states)
%         plot(results.time,results.statevalues(:,i))
%         title(results.states(i));
%     else
%         plot(results.time,results.variablevalues(:,i-length(results.states)))
%         title(results.variables(i-length(results.states)))
%     end
% end

if isempty(sgRNACasesValue)
figure(200)
% hold on

subplot(length(araIdxData),3,A+1)
errorbar(DAta.timepoints(idxTimeData),DAta.mKO2.(convertCharsToStrings(araChar))(idxTimeData)./DAta.mKO2.(convertCharsToStrings('ara16'))(idxTimeData),fluoStDev(:,1),'color', [200/255 200/255 10/255]);
hold on
plot(results.time(idxsimulation),simulationMeas(idxsimulation,1),'k');
title(sprintf('mKO2-%s',araChar{:}));

subplot(length(araIdxData),3,A+2)
errorbar(DAta.timepoints(idxTimeData),DAta.mKate2.(convertCharsToStrings(araChar))(idxTimeData)./DAta.mKate2.(convertCharsToStrings('ara16'))(idxTimeData),fluoStDev(:,2),'color', [0 0.4470 0.7410]);
hold on
plot(results.time(idxsimulation),simulationMeas(idxsimulation,2),'k');
title(sprintf('mKate2-%s',araChar{:}));

subplot(length(araIdxData),3,A+3)
errorbar(DAta.timepoints(idxTimeData),DAta.GFP.(convertCharsToStrings(araChar))(idxTimeData)./DAta.GFP.(convertCharsToStrings('ara16'))(idxTimeData),fluoStDev(:,3),'color', [0 0.5 0]);
hold on
plot(results.time(idxsimulation),simulationMeas(idxsimulation,3),'k');
title(sprintf('GFP-%s %.1f',araChar{:},objfcost));

else 
    figure(sgRNACasesValue+200)

subplot(length(araIdxData),2,A+1)
errorbar(DAta.timepoints(idxTimeData),DAta.mKO2.(convertCharsToStrings(araChar))(idxTimeData,sgRNACasesValue)./DAta.mKO2.(convertCharsToStrings('ara9'))(idxTimeData,sgRNACasesValue),fluoStDev(:,1),'color', [200/255 200/255 10/255]);
hold on
plot(results.time(idxsimulation),simulationMeas(idxsimulation,1),'k');
title(sprintf('mKO2-sgRNA%g-%s',sgRNACasesValue,araChar{:}));

subplot(length(araIdxData),2,A+2)
errorbar(DAta.timepoints(idxTimeData),DAta.GFP.(convertCharsToStrings(araChar))(idxTimeData,sgRNACasesValue)./DAta.GFP.(convertCharsToStrings('ara9'))(idxTimeData,sgRNACasesValue),fluoStDev(:,2),'color', [0 0.5 0]);
hold on
plot(results.time(idxsimulation),simulationMeas(idxsimulation,2),'k');
title(sprintf('GFP-sgRNA%g-%s %.1f',sgRNACasesValue,araChar{:},objfcost));

    
end