%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function parsave(fname,data)

var_name=genvarname(inputname(2));
eval([var_name '=data'])

try
    save(fname,var_name,'-append')
catch
    save(fname,var_name)
end