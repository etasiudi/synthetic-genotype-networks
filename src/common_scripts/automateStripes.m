%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************

function [ABC,percent] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel)

switch thisModel
    case {'node3_caseA' , 'node3_caseA_Bl_Ch' , 'node3_caseA_Bm_Ch'} 
        run run_for_stripes_CaseA
        
    case {'node3_caseB' , 'node3_caseB_Bl_Ch' , 'node3_caseB_Bh_Cl'}      
        run run_for_stripes_CaseB
        
    case 'node3_caseC'        
        run run_for_stripes_CaseC

    case {'node3_caseD' , 'node3_caseD_Bl_Ch'}       
        run run_for_stripes_CaseD
    
    case 'node3_caseE'
        run run_for_stripes_CaseE
        
    case 'node3_caseF'
        run run_for_stripes_CaseF       
 
        if isnan(percent)
            percent = NAN;
        end
            
end