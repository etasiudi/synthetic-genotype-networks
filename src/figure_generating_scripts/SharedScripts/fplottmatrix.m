%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi, J Stelling
%>  ***********************************************************************
% Function: Plot transition matrices

function varargout = fplottmatrix(M,S,phenoID,cmax,fstring)

nP = length(phenoID);

imagesc(M');
xlabel('From phenotype');
ylabel('To phenotype');

h=gca;
h.YAxis.Direction='normal';
h.XTick=1:nP;
h.XTickLabel=phenoID;
h.YTick=1:nP;
h.YTickLabel=phenoID;
% cmap = brewermap(50,'Purples');
cmap = brewermap(50,'Reds');

colormap(cmap)
caxis([0 cmax]);
hold on;

f = ['%' fstring ' (%' fstring ')'];

for zf = 1:nP
    for zp = 1:nP
        s = sprintf(f,M(zf,zp),S(zf,zp));
        text(zf,zp,s,'HorizontalAlignment','center','Color',.0*ones(1,3), ...
            'FontSize',8);
    end
end

varargout{1} = h;

return