%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi, J Stelling
%>  ***********************************************************************
%% Function: Generalized logistic, superposition

function [y,varargout] = fsigmoid(para,t)

s = [fs(para(2:5),t) fs(para(6:9),t) fs(para(10:13),t)];
y = para(1) + sum(s,2);

varargout{1} = s;
varargout{2} = [fdiff(para(2:5),t) + fdiff(para(6:9),t) + fdiff(para(10:13),t)];

return

% function for individual sigmoids
function ys = fs(para,t)

ys = (para(1)./((1 + para(2)*exp(-para(3)*t)).^(1/para(4))));

return

% function for time derivative
function ysdiff = fdiff(para,t)

ysdiff = (para(1)*para(2)*para(3)*exp(-para(3)*t))./(para(4)*(para(2)*exp(-para(3)*t) + 1).^(1/para(4) + 1));

return
