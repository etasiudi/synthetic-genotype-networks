%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi, J Stelling
%>  ***********************************************************************
function obj = fobjod(para,varargin)

persistent t m s

if nargin > 1
    t = varargin{1};
    m = varargin{2};
    s = varargin{3};
    obj = nan;
  
else    
    mi  = fsigmoid(para,t);
    obj = sum((mi-m).^2./(s.^2)); 
%     obj = sum((mi-m).^2);
end

return
