%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
clear all;close all
data_to_use = '.\data\3node\4.1\unormalized_data_41.mat';
load(data_to_use)

% raw data
for i = 1:16
ODm(:,i)  = OD.(sprintf('ara%d',i));
ODsd(:,i) = ODstd.(sprintf('ara%d',i));
end
ODt  = [10:10:10*length(ODm(:,1))]';
n    = size(ODm,2);
la   = fieldnames(OD);


% data plots

%% if using average Data have to switch to 98 tp instead of 100
qwe = 90 %;
% qwe = 100;
  % Press a key here.You can see the message 'Paused: Press any key' in        % the lower left corner of MATLAB window.
disp(qwe)
% pause;
% disp('Press a key !')
figure(1);
clf;
r = [1 8 16];
titlenames = {'Ara High','Ara Med','Ara Low'};

for z = 1:3
    subplot(1,3,z);
    errorbar(ODt(1:qwe),ODm(1:qwe,r(z)),ODsd(1:qwe,r(z)),'.');
%     title(la{r(z)});
title(titlenames{z});
        ax = gca; 
    ax.FontSize = 12; 
    hold on
end
OD100tm = ODm(1:qwe,:);
OD100tsd = ODsd(1:qwe,:);
ODt = ODt(1:qwe);
load('.\src\growth_related_opt\3_node\growth_param_41.mat')
para1 = growth_param_41;
% plot model vs approximation
r = [1 8 16];
for j = 1:3
[ODemi,ODsig,~] = fsigmoid(para1(r(j),:),ODt);
subplot(1,3,j)
plot(ODt,ODemi,'-','LineWidth',1);

hold on
for z = 1:size(ODsig,2)
    plot(ODt,ODsig,'-');
end
xlabel('Time (min)');
ylabel('Avg. OD (-)');
xlim([0 900])
    ax = gca; 
    ax.FontSize = 12; 
end

fortitlecaseName = 'growth_41';
h = figure(1)
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,sprintf('Circuit %s B',fortitlecaseName),'-dpdf','-r0')