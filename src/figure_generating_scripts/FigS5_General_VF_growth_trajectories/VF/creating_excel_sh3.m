%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% create excel variance data
clear all;close all

%% for GFP fig
dinfo = dir('.\\data\\3node\\');
dinfo(ismember( {dinfo.name}, {'.', '..'})) = [];
names = {dinfo.name};
% names = names(1:4)
% namesWODot = erase(names,"."),namesWODot{i} unormalized_data_%s
pathForODdata = '.\\data\\3node\\';
Ara = sprintfc('ara%d',[1:16]);

GFPpre.ara1 = zeros(98,1);
GFPpre.ara2 = zeros(98,1);
GFPpre.ara3 = zeros(98,1);
GFPpre.ara4 = zeros(98,1);
GFPpre.ara5 = zeros(98,1);
GFPpre.ara6 = zeros(98,1);
GFPpre.ara7 = zeros(98,1);
GFPpre.ara8 = zeros(98,1);
GFPpre.ara9 = zeros(98,1);
GFPpre.ara10 = zeros(98,1);
GFPpre.ara11 = zeros(98,1);
GFPpre.ara12 = zeros(98,1);
GFPpre.ara13 = zeros(98,1);
GFPpre.ara14 = zeros(98,1);
GFPpre.ara15 = zeros(98,1);
GFPpre.ara16 = zeros(98,1);

[~,LIB] = ismember(names,names);

for i = 1: length(LIB)
    filePattern = fullfile(sprintf([pathForODdata '%s\\'],names{LIB(i)}));
    fileList = dir(fullfile(filePattern, '*.mat'));
    load(sprintf([pathForODdata '%s\\%s'],names{LIB(i)},fileList.name));
    for j = 1:16
        GFPpre.(Ara{j})(1:98,i) = GFP.(Ara{j})(1:98);
        GFPpreStd.(Ara{j})(1:98,i) = GFPstd.(Ara{j})(1:98);
    end
end


h = 98;
GFPdataNoreshape = zeros(h,20,16);
GFPdataNoreshapeStd = zeros(h,20,16);

for i = 1:16
    GFPdataNoreshape(:,:,i) = [GFPpre.(Ara{i})(1:98,:)];
    GFPdataNoreshapeStd(:,:,i) = [GFPpreStd.(Ara{i})(1:98,:)];
end

GFP_data = reshape(GFPdataNoreshape,16,h*20);
GFPstd_data = reshape(GFPdataNoreshapeStd,16,h*20);
hGFP = histogram(GFP_data,10000);%,31360
colormap = cbrewer('seq','Greens',24,'cubic');

binnedGFPstdev = zeros(hGFP.NumBins,1);
binnedGFP = zeros(hGFP.NumBins,1);


for j = 1: hGFP.NumBins
   [row,col] = find(hGFP.BinEdges(j) <= hGFP.Data& hGFP.Data< hGFP.BinEdges(j+1));
   if ~isempty(col)
   for i = 1:length(row)
   C(i) = (hGFP.Data(row(i),col(i)));
   D(i) = GFPstd_data(row(i),col(i));
   end
   binnedGFP(j) = mean(C);
   binnedGFPstdev(j) = mean((D));
   %coefVar(j) = binnedGFPstdev(j)/binnedGFP(j);

   clear C D;
   else continue
   end
    
end
[Lr2,~] = find(binnedGFP>=7*10^4 & binnedGFP<=2.5*10^5);
subplot(1,2,1)
hold on
plot((binnedGFP),binnedGFPstdev./binnedGFP,'.','color',colormap(18,:),'MarkerSize',10);
plot((binnedGFP(Lr2)),binnedGFPstdev(Lr2)./binnedGFP(Lr2),'.k');
legend({'data','between 7e04 and 2.5e05 AU'},'Location','best');
xlabel('GFP');
ylabel('CoefVar of GFP');
ax = gca; 
ax.FontSize = 12; 

A = binnedGFP;
B = binnedGFPstdev;
[~,Lr] = find(A>=7*10^4& A<=2.5e+05);
k = 1;
x = A(Lr);
y = B(Lr);
sample = [min(A):max(A)];
[constpart1GFP] = find(sample<7*10^4);
constStDGFP = zeros(1,length(constpart1GFP)) + k*10350;
[otherGFP] = find(sample>=7*10^4 & sample<=2.5e+05);
stdGFPb = [constStDGFP,k*(0.13*(sample(otherGFP)) + 1250)];
[largerGFP] = find(sample>2.5e+05);
constupdStDGFP= zeros(1,length(largerGFP)) + k*3.3750e+04;
stdGFP = [stdGFPb,constupdStDGFP];

subplot(1,2,2)
box off
hold on
plot((A),(B),'.','color',colormap(18,:),'MarkerSize',12)
plot((sample),(stdGFP),'-k','LineWidth',2)
legend({'data','model'},'Location','best')
xlabel('GFP');
ylabel('stdev GFP');
ax = gca; 
ax.FontSize = 12; 
h = figure(1);
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'GFP_VarianceForAll_B','-dpdf','-r0')

clear all;close all

%% mKO2 variance
%% for mko2 fig
dinfo = dir('.\\data\\3node\\');
dinfo(ismember( {dinfo.name}, {'.', '..'})) = [];
names = {dinfo.name};
% names = names(1:4)
% namesWODot = erase(names,"."),namesWODot{i} unormalized_data_%s
pathForODdata = '.\\Data\\3node\\';
Ara = sprintfc('ara%d',[1:16]);

GFPpre.ara1 = zeros(98,1);
GFPpre.ara2 = zeros(98,1);
GFPpre.ara3 = zeros(98,1);
GFPpre.ara4 = zeros(98,1);
GFPpre.ara5 = zeros(98,1);
GFPpre.ara6 = zeros(98,1);
GFPpre.ara7 = zeros(98,1);
GFPpre.ara8 = zeros(98,1);
GFPpre.ara9 = zeros(98,1);
GFPpre.ara10 = zeros(98,1);
GFPpre.ara11 = zeros(98,1);
GFPpre.ara12 = zeros(98,1);
GFPpre.ara13 = zeros(98,1);
GFPpre.ara14 = zeros(98,1);
GFPpre.ara15 = zeros(98,1);
GFPpre.ara16 = zeros(98,1);

[~,LIB] = ismember(names,names);

for i = 1: length(LIB)
    filePattern = fullfile(sprintf([pathForODdata '%s\\'],names{LIB(i)}));
    fileList = dir(fullfile(filePattern, '*.mat'));
    load(sprintf([pathForODdata '%s\\%s'],names{LIB(i)},fileList.name));    for j = 1:16
        GFPpre.(Ara{j})(1:98,i) = mKO2.(Ara{j})(1:98);
        GFPpreStd.(Ara{j})(1:98,i) = mKO2std.(Ara{j})(1:98);
    end
end


h = 98;
GFPdataNoreshape = zeros(h,20,16);
GFPdataNoreshapeStd = zeros(h,20,16);

for i = 1:16
    GFPdataNoreshape(:,:,i) = [GFPpre.(Ara{i})(1:98,:)];
    GFPdataNoreshapeStd(:,:,i) = [GFPpreStd.(Ara{i})(1:98,:)];
end

GFP_data = reshape(GFPdataNoreshape,16,h*20);
GFPstd_data = reshape(GFPdataNoreshapeStd,16,h*20);
hGFP = histogram(GFP_data,10000);
colormap = cbrewer('seq','Oranges',24,'cubic');

binnedGFPstdev = zeros(hGFP.NumBins,1);
binnedGFP = zeros(hGFP.NumBins,1);


for j = 1: hGFP.NumBins
   [row,col] = find(hGFP.BinEdges(j) <= hGFP.Data& hGFP.Data< hGFP.BinEdges(j+1));
   if ~isempty(col)
   for i = 1:length(row)
   C(i) = (hGFP.Data(row(i),col(i)));
   D(i) = GFPstd_data(row(i),col(i));
   end
   binnedGFP(j) = mean(C);
   binnedGFPstdev(j) = mean((D));
   %coefVar(j) = binnedGFPstdev(j)/binnedGFP(j);

   clear C D;
   else continue
   end
    
end
[Lr2,~] = find(binnedGFP>=6*10^3 & binnedGFP<=2e+04);
subplot(1,2,1)
hold on
plot((binnedGFP),binnedGFPstdev./binnedGFP,'.','color',colormap(18,:),'MarkerSize',10);
plot((binnedGFP(Lr2)),binnedGFPstdev(Lr2)./binnedGFP(Lr2),'.k');
legend({'data','between 6e03 and 2e04 AU'},'Location','best');
xlabel('mKO2');
ylabel('CoefVar of mKO2');
ax = gca; 
ax.FontSize = 12; 
A = binnedGFP;
B = binnedGFPstdev;
[~,Lr] = find(A>=6*10^3& A<=2e+04);
k = 1;
x = A(Lr);
y = B(Lr);
sample = [min(A):max(A)];
[constpart1GFP] = find(sample<6*10^3);
constStDGFP = zeros(1,length(constpart1GFP)) + k*899;
[otherGFP] = find(sample>=6*10^3 & sample<=2e+04);
stdGFPb = [constStDGFP,k*(0.08*(sample(otherGFP)) + 419)];
[largerGFP] = find(sample>2e+04);
constupdStDGFP= zeros(1,length(largerGFP)) + k*2.0189e+03;
stdGFP = [stdGFPb,constupdStDGFP];

subplot(1,2,2)
box off
hold on
plot((A),(B),'.','color',colormap(18,:),'MarkerSize',12)
plot((sample),(stdGFP),'-k','LineWidth',2)
legend({'data','model'},'Location','best')
xlabel('mKO2');
ylabel('stdev mKO2');
ax = gca; 
ax.FontSize = 12; 
h = figure(1);
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'mKO2_VarianceForAll_B','-dpdf','-r0')

clear all;close all


%% mKate2 figure

%% for mKate2 fig
dinfo = dir('.\\data\\3node\\');
dinfo(ismember( {dinfo.name}, {'.', '..'})) = [];
names = {dinfo.name};
% names = names(1:4)
% namesWODot = erase(names,"."),namesWODot{i} unormalized_data_%s
pathForODdata = '.\\Data\\3node\\';
Ara = sprintfc('ara%d',[1:16]);

GFPpre.ara1 = zeros(98,1);
GFPpre.ara2 = zeros(98,1);
GFPpre.ara3 = zeros(98,1);
GFPpre.ara4 = zeros(98,1);
GFPpre.ara5 = zeros(98,1);
GFPpre.ara6 = zeros(98,1);
GFPpre.ara7 = zeros(98,1);
GFPpre.ara8 = zeros(98,1);
GFPpre.ara9 = zeros(98,1);
GFPpre.ara10 = zeros(98,1);
GFPpre.ara11 = zeros(98,1);
GFPpre.ara12 = zeros(98,1);
GFPpre.ara13 = zeros(98,1);
GFPpre.ara14 = zeros(98,1);
GFPpre.ara15 = zeros(98,1);
GFPpre.ara16 = zeros(98,1);

[~,LIB] = ismember(names,names);

for i = 1: length(LIB)
%     load(sprintf([pathForODdata '%s\\mKate2data.mat'],names{LIB(i)}));
    filePattern = fullfile(sprintf([pathForODdata '%s\\'],names{LIB(i)}));
    fileList = dir(fullfile(filePattern, '*.mat'));
    load(sprintf([pathForODdata '%s\\%s'],names{LIB(i)},fileList.name));
    for j = 1:16
        GFPpre.(Ara{j})(1:98,i) = mKate2.(Ara{j})(1:98);
        GFPpreStd.(Ara{j})(1:98,i) = mKate2std.(Ara{j})(1:98);
    end
end


h = 98;
GFPdataNoreshape = zeros(h,20,16);
GFPdataNoreshapeStd = zeros(h,20,16);

for i = 1:16
    GFPdataNoreshape(:,:,i) = [GFPpre.(Ara{i})(1:98,:)];
    GFPdataNoreshapeStd(:,:,i) = [GFPpreStd.(Ara{i})(1:98,:)];
end

GFP_data = reshape(GFPdataNoreshape,16,h*20);
GFPstd_data = reshape(GFPdataNoreshapeStd,16,h*20);
hGFP = histogram(GFP_data,10000);
colormap = cbrewer('seq','Blues',24,'cubic');

binnedGFPstdev = zeros(hGFP.NumBins,1);
binnedGFP = zeros(hGFP.NumBins,1);


for j = 1: hGFP.NumBins
   [row,col] = find(hGFP.BinEdges(j) <= hGFP.Data& hGFP.Data< hGFP.BinEdges(j+1));
   if ~isempty(col)
   for i = 1:length(row)
   C(i) = (hGFP.Data(row(i),col(i)));
   D(i) = GFPstd_data(row(i),col(i));
   end
   binnedGFP(j) = mean(C);
   binnedGFPstdev(j) = mean((D));
   %coefVar(j) = binnedGFPstdev(j)/binnedGFP(j);

   clear C D;
   else continue
   end
    
end
[Lr2,~] = find(binnedGFP>=500);
subplot(1,2,1)
hold on
plot((binnedGFP),binnedGFPstdev./binnedGFP,'.','color',colormap(18,:),'MarkerSize',10);
plot((binnedGFP(Lr2)),binnedGFPstdev(Lr2)./binnedGFP(Lr2),'.k');
legend({'data','above 500 AU'},'Location','best');
xlabel('mKate2');
ylabel('CoefVar of mKate2');
ax = gca; 
ax.FontSize = 12; 
A = binnedGFP;
B = binnedGFPstdev;
[~,Lr] = find(A>=500);
k = 1;
x = A(Lr);
y = B(Lr);
sample = [min(A):max(A)];
[constpart1GFP] = find(sample<500);
constStDGFP = zeros(1,length(constpart1GFP)) + k*43;
[otherGFP] = find(sample>=500);
stdGFPb = [constStDGFP,k*(0.03*(sample(otherGFP)) + 28)];
% [largerGFP] = find(sample>2e+04);
% constupdStDGFP= zeros(1,length(largerGFP)) + k*2.0189e+03;
stdGFP = [stdGFPb]; %,constupdStDGFP

subplot(1,2,2)
box off
hold on
plot((A),(B),'.','color',colormap(18,:),'MarkerSize',12)
plot((sample),(stdGFP),'-k','LineWidth',2)
legend({'data','model'},'Location','best')
xlabel('mKate2');
ylabel('stdev mKate2');
ax = gca; 
ax.FontSize = 12;
h = figure(1);
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'mKate2_VarianceForAll_B','-dpdf','-r0')
close all;clear all

