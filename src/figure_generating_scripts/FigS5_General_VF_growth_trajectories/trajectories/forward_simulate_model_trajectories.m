%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
clear;close all
paramSpecs = readtable('.\src\parameters\paramSpecs_for_optimization_Updated.txt');

thisModel = {'node3_caseB','node3_caseF'};
thisData = {'./data/3node/2c.4/unormalized_data_2c4.mat';...
    './data/3node/4.1/unormalized_data_41.mat'};
GrowthParam = {    './src/growth_related_opt/3_node/growth_param_e4.mat';...
    './src/growth_related_opt/3_node/growth_param_41.mat'};

node3Seq = { {'sg1t4','sg2','sg1','sg2'},...
    {'sg1','sg2','sg1','sg4','sg4','sg3'}};

timepointExp = {[10:10:900]',[10:10:900]'};
% 
araTotal3node = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];
araTotal2node = [13321.788,13.322,6.661,3.33,1.665,0.833,0.416,0.208,0];
arabinoseExp = {araTotal3node([1]),araTotal3node([1])};


[~, ~, A, paramValueNotFixed, ID] = getFixedAndNonFixed(paramSpecs);
FlPlot = true;
sgRNACasesNames = {[],[]};


jointOpt = true;
setObjectiveSimulation(paramSpecs.p0(ID), paramSpecs, FlPlot, arabinoseExp, timepointExp...
,thisModel, thisData,GrowthParam,sgRNACasesNames,jointOpt,node3Seq);

% print(figure(), '-painters', '-dpdf', 'PredictedTrajectory-41.pdf') %works perfect
