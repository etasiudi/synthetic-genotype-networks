%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************


clear all;close all
addpath('.\src\common_scripts')
addpath('.\src\models\models_for_optimization')
addpath('.\src\parameters')
paramSpecs = readtable('C:\Users\etasiudi\Documents\MATLAB\ver11_CRISPRi_and_GPmaps\synthetic-genotype-networks-main\src\parameters\paramSpecs_for_optimization_Updated.txt');

% c1.3
thisModel = 'node3_caseA_Bm_Ch';
thisData  = './data/3node/1.3/unormalized_data_13.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_AVGcaseA.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);


% 2c6
thisModel = 'node3_caseB';
thisData  = './data/3node/2c.6/unormalized_data_2c6.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
