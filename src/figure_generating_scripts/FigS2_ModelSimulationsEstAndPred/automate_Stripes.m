
%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************


%% automate the figure generation for predictions

clear all;close all
addpath('.\src\common_scripts')
addpath('.\src\models\models_for_optimization')
addpath('.\src\parameters')

paramSpecs = readtable('C:\Users\etasiudi\Documents\MATLAB\ver11_CRISPRi_and_GPmaps\synthetic-genotype-networks-main\src\parameters\paramSpecs_for_optimization_Updated.txt');
%% caseA
% c1.4
circuitName = {'c14'}; 
thisModel = 'node3_caseA';
thisData  = './data/3node/1.4/unormalized_data_14.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_c14.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = b;

% c1.3
circuitName{end+1} = 'c13'; 
thisModel = 'node3_caseA_Bm_Ch';
thisData  = './data/3node/1.3/unormalized_data_13.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_AVGcaseA.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% c1.1
circuitName{end+1} = 'c11'; 
thisModel = 'node3_caseA_Bl_Ch';
thisData  = './data/3node/1.1/unormalized_data_11.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_AVGcaseA.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% c1.2
circuitName{end+1} = 'c12'; 
thisModel = 'node3_caseA_Bl_Ch';
thisData  = './data/3node/1.2/unormalized_data_12.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_AVGcaseA.mat';
sequenceSgRNA = {'sg1','sg2','sg1'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

%% case B
% 2c1
circuitName{end+1} = 'c2c1'; 
thisModel = 'node3_caseB_Bl_Ch';
thisData  = './data/3node/2c.1/unormalized_data_2c1.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c2
circuitName{end+1} = 'c2c2'; 
thisModel = 'node3_caseB_Bl_Ch';
thisData  = './data/3node/2c.2/unormalized_data_2c2.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4t4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c3
circuitName{end+1} = 'c2c3'; 
thisModel = 'node3_caseB';
thisData  = './data/3node/2c.3/unormalized_data_2c3.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4t4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c4
circuitName{end+1} = 'c2c4'; 
thisModel = 'node3_caseB';
thisData  = './data/3node/2c.4/unormalized_data_2c4.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg2'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c5
circuitName{end+1} = 'c2c5'; 
thisModel = 'node3_caseB';
thisData  = './data/3node/2c.5/unormalized_data_2c5.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1','sg2','sg1','sg2'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c6
circuitName{end+1} = 'c2c6'; 
thisModel = 'node3_caseB';
thisData  = './data/3node/2c.6/unormalized_data_2c6.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c7
circuitName{end+1} = 'c2c7'; 
thisModel = 'node3_caseB_Bh_Cl';
thisData  = './data/3node/2c.7/unormalized_data_2c7.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2c8
circuitName{end+1} = 'c2c8'; 
thisModel = 'node3_caseB_Bh_Cl';
thisData  = './data/3node/2c.8/unormalized_data_2c8.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4t4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

%% caseC

% 2b1
circuitName{end+1} = 'c2b1'; 
thisModel = 'node3_caseC';
thisData  = './data/3node/2b.1/unormalized_data_2b1.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4t4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% 2b2
circuitName{end+1} = 'c2b2'; 
thisModel = 'node3_caseC';
thisData  = './data/3node/2b.2/unormalized_data_2b2.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

%% case E
% 2a1
circuitName{end+1} = 'c2a1'; 
thisModel = 'node3_caseE';
thisData  = './data/3node/2a.1/unormalized_data_2a1.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4t4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

%% case D
% c3.1
circuitName{end+1} = 'cc3.1'; 
thisModel = 'node3_caseD';
thisData  = './data/3node/3.1/unormalized_data_31.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_AVGcaseD.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% c3.2
circuitName{end+1} = 'cc3.2'; 
thisModel = 'node3_caseD';
thisData  = './data/3node/3.2/unormalized_data_32.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_32.mat';
sequenceSgRNA = {'sg1','sg2','sg1','sg4','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% c3.3
circuitName{end+1} = 'cc3.3'; 
thisModel = 'node3_caseD_Bl_Ch';
thisData  = './data/3node/3.3/unormalized_data_33.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_AVGcaseD.mat';
sequenceSgRNA = {'sg1t4','sg2','sg1','sg4t4','sg4t4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

%% caseF
% c4.1
circuitName{end+1} = 'c4.1'; 
thisModel = 'node3_caseF';
thisData  = './data/3node/4.1/unormalized_data_41.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_41.mat';
sequenceSgRNA = {'sg1','sg2','sg1','sg4','sg4','sg3'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];


%% control (caseB/2c-NF1)

circuitName{end+1} = 'c2cNF1'; 
thisModel = 'node3_caseB';
thisData  = './data/3node/2c-NF.1/unormalized_data_2cNF1.mat';
GrowthParam = './src/growth_related_opt/3_node/growth_param_2cNF1.mat';
sequenceSgRNA = {'sg1','sg2','sg1','sg4'};
[~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
percent = [percent b];

% % %% prediction
% % % 2c-P1
% circuitName{end+1} = 'prediction'; 
% thisModel = 'node3_caseB';
% thisData  = './data/3node/2c.3/unormalized_data_2c3.mat';
% GrowthParam = './src/growth_related_opt/3_node/growth_param_e4.mat';
% sequenceSgRNA = {'sg1t4','sg4','sg3','sg2'};
% [~,b] = automateStripes(paramSpecs,thisData, GrowthParam, sequenceSgRNA, thisModel);
% percent = [percent b];



percent = percent';
circuitName = circuitName';