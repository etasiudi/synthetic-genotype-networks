%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
close all;clear
dataB = readtable('.\data\ForFigures\neutrality_position_startingAtGreen_Update_20k.csv');
% dataB(:,19:end) = [];
% dataD = readtable('neutrality_position_startingAtNF_new.csv');
dataD10k = [dataB];

idx = [500 2000 20000];
% figure
for i = 1:8
    for j = 1:length(idx)
    m1(i,j) = mean (table2array(dataD10k(1:idx(j),i+2)));
    m1std(i,j) = std(table2array(dataD10k(1:idx(j),i+2)));
    m2(i,j) = mean(table2array(dataD10k(1:idx(j),i+10)));
    m2std(i,j) = std(table2array(dataD10k(1:idx(j),i+10)));
    end
    z(i,:) = m1(i,:)./m2(i,:);
    deltaz(i,:) = z(i,:).*(sqrt((m1std(i,:)./m1(i,:)).^2 + (m2std(i,:)./m2(i,:)).^2));
%     subplot(2,4,i)
%     errorbar(idx,z(i,:),deltaz(i,:),'.')
%     title(i)
    
end
 
z = round(z',2);
deltaz = round(deltaz',2);