%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% script for path length

clear all;close all

% General colors used:  [0, 174/255, 239/255;7/255, 146/255, 71/255 ;0.8*ones(1,3)]; % colors
%
% fnames = {'.\Data\ForFigures\PathCircuits_startingFromBlue.csv'}; % data files

%for Blue
% cols   = [0.8*ones(1,3);7/255, 146/255, 71/255;]; % colors
% 
fnames = {'.\Data\ForFigures\PathCircuits_startingFromGreen.csv'}; % data files
% %for Green
cols   = [0.8*ones(1,3);0, 174/255, 239/255]; % colors

% fnames = {'.\data\ForFigures\PathCircuits_startNFWalk2.csv'}; % data files
% %for NF
% cols   = [0, 174/255, 239/255;7/255, 146/255, 71/255]; % colors
for z = 1:length(fnames)
    if ~exist('T','var') || length(T) < length(fnames)
        T{z} = readtable(fnames{z});
    end
    
    vn = T{z}.Properties.VariableNames;
    v  = T{z}.(vn{3});
    c  = categorical(T{z}.(vn{2}));
    
    figure(z);
    clf;
    h = violinplot(v,c,'BoxColor',[0 0 0],'ShowNotches',true,'ShowMean',true,'ViolinAlpha',0.05);
    for zv = 1:length(h)
        [h(zv).ViolinColor,h(zv).EdgeColor] = deal(cols(zv,:));
    end
end
print(gcf, '-painters', '-dpdf', 'PathCircuits_startingFromGreen-Exper.pdf') %works perfect
