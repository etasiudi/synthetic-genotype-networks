%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% Script CRISPRi epistasis analysis plots

addpath lib\Violinplot-Matlab-master\; % violin plot package

fnames = {'.\data\ForFigures\EpistasisCircuits.csv'}; % data files
cols   = [0, 174/255, 239/255;7/255, 146/255, 71/255; 0.8*ones(1,3)]; % colors


for z = 1:length(fnames)
    if ~exist('T','var') || length(T) < length(fnames)
        T{z} = readtable(fnames{z});
    end
    
    vn = T{z}.Properties.VariableNames;
    v  = T{z}.(vn{2});
    c  = categorical(T{z}.(vn{3}));
    
    figure(z);
    clf;
    h = violinplot(v,c,'BoxColor',[0 0 0],'ShowNotches',true,'ShowMean',false,'ViolinAlpha',0.05);
    for zv = 1:length(h)
        [h(zv).ViolinColor,h(zv).EdgeColor] = deal(cols(zv,:));
    end
end
print(gcf, '-painters', '-dpdf', 'Epistasis-Exper.pdf') %works perfect
