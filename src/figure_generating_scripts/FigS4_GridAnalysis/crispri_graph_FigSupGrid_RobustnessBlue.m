%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi, J Stelling
%>  ***********************************************************************
phenoID = {'NF','Blue','Green'};
shortID = {'N','B','G'};
cols    = [0.8*ones(1,3); 0, 0.4470, 0.7410;0.4660, 0.6740, 0.1880]; % colors

addpath .\lib\colorBrewer

if ~exist('TN','var')
    fprintf('Reading node data.\n');
    TN = readtable('.\data\ForFigures\NodesTable_ControlStripe.csv','Delimiter',',','ReadVariableNames',true);

    TN.ID = uint32(TN.ID);
    TN.StripeForm = uint8(TN.StripeForm);
    v = cellfun(@str2num,TN.Name,'UniformOutput',false);
    circuitName = uint8(cell2mat(v));
    [topoName,~,circuitTopo] = unique(circuitName(:,1:6) > 0,'rows');
    circuitTopo = uint8(circuitTopo);
    TN.Name = [];
end
if ~exist('TE','var')
    fprintf('Reading edge data.\n');
    TE = readtable('.\data\ForFigures\EdgesTableUNIQUE_ControlStripe.csv');
    TE.From = uint32(TE.From);
    TE.To = uint32(TE.To);
end

nT = size(topoName,1);
nP = length(phenoID);

% construct graph
if ~exist('G','var')
    G = graph(TE.From,TE.To);
end
nN = G.numnodes;
%% Compute robustness / number of neighbors per phenotype
% and number of neighbors per phenotype given a topology

sF = TN.StripeForm;
nFrom = TE.From;
nTo   = TE.To;

if ~exist('robustG','var')
    fprintf('Analyzing robustness / evolvability: ');
    dG      = degree(G); % degree
    robustG = nan(nN,1); % robustness: fraction neighbors w/ same genotype
%     adjG    = nan(nN,nP); % fraction of adjacent phenotypes
%     evolG   = nan(nN,1); % evolvability: # new adjacent phenotypes per genotype
%     evolGT  = nan(nN,1); % evolvability: # new adjacent phenotypes per genotype in same topology
    
    [nNeighborPheno,nNeighborPhenoInT] = deal(nan(nN,nP));
        
    for z = 1:nN
        idx   = neighbors(G,z);
        
        pheno     = sF(idx);
        pheno0    = sF(z);
        topo      = circuitTopo(idx);
        topo0     = circuitTopo(z);
        tidx      = topo==topo0;
        
        robustG(z) = sum(pheno==pheno0) / dG(z);
%         evolG(z)   = numel(unique(pheno));
%         evolGT(z)  = numel(unique(pheno(tidx)));
        
        for zp = 1:nP
            pidx = pheno==zp;
%             adjG(z,zp)              = sum(pidx) / dG(z);
            nNeighborPheno(z,zp)    = sum(pidx);
            nNeighborPhenoInT(z,zp) = sum(pidx & tidx);
        end
        
        if ~mod(z,floor(nN/20))
            fprintf('.'); 
        end
    end
    fprintf('\n');
end

%% Topology transitions

if ~exist('topoTrans','var')
    topoFrom = circuitTopo(nFrom);
    topoTo   = circuitTopo(nTo);
    
    topoTrans = nan(nT);
    fprintf('Analyzing topology transitions: ');
    for z1 = 1:nT
        idx1 = topoFrom==z1;
        for z2 = 1:nT
            idx2 = topoTo==z2;
            topoTrans(z1,z2) = sum(idx1 & idx2);
        end
        fprintf('.');
    end
    fprintf('\n');
end

%% Robustness by topology
% clf;
% figure()
clusterID1 = [0.8*ones(1,3); 0, 0.4470, 0.7410;0.4660, 0.6740, 0.1880]; 
% brewermap(3,'OrRd');
for t = 1:nT
    idx = circuitTopo==t;
    RobT(t,:) = mean(nNeighborPheno(idx,:)./sum(nNeighborPheno(idx,:),2));
    RobinT(t,:) = mean(nNeighborPhenoInT(idx,:)./sum(nNeighborPhenoInT(idx,:),2));
    
end
% for zv = 1:2
% subplot(2,1,zv);
% if zv == 1
%     for z = 1:nP
%         scatter((1:nT'),RobT(:,z),20,clusterID1(z,:),'filled');
%         hold on;
%     end
%     xlim(.5+[0 nT]);
%     ylabel('Mean Rob Topo');
% else
%     for z = 1:nP
%         scatter((1:nT'),RobinT(:,z),20,clusterID1(z,:),'filled');
%         hold on;
%     end
%     xlim(.5+[0 nT]);
%     ylabel('Mean Rob within same Topo');
% end
% end

figure();
clf;
hold on
% connections between topologies
[idx1,idx2] = find(topoTrans>0);
m = 40*topoTrans/max(max(topoTrans));
% m = 5*topoTrans ./ (sum(topoTrans,2)*ones(1,nT));
for p = 2
    mAvgRob = [RobT(:,p),RobinT(:,p)];
    rng('default')
%     subplot(3,1,p)
for z = 1:length(idx1)
    idx = [idx1(z) idx2(z)];
    h = plot(mAvgRob(idx,1),mAvgRob(idx,2),'k-');
    hold on
    h.LineWidth = max(0.1,min(3,m(idx1(z),idx2(z))));
    h.Color = 0.75*ones(1,3);
end
nClust = 5;
% clusterID = clusterdata(mAvgRob,'Criterion','distance','Cutoff',0.1);
c = brewermap(nClust,'Set1');%nClust
cols = c(clusterID,:);
% % 
nEdges = sum(topoName,2);
sz = 5*max(5,100*(nEdges/max(nEdges)).^2);

nEdges = sum(topoName,2);
sz = 5*max(5,100*(nEdges/max(nEdges)).^2);
% sz = 10*max(5,100*(nGenoInT/max(nGenoInT)));
h = scatter(mAvgRob(:,1),mAvgRob(:,2),sz,cols,'filled');
h.MarkerFaceAlpha = 0.1;
h.MarkerEdgeColor = [0 0 0]; %'flat';
h.MarkerEdgeAlpha = 1;
axis equal
v = [0 1];%[0 0.5];
axis([v v])
hold on;
plot(v,v,'k--')
xlabel('Robustness');
ylabel('Robustness, fixed topology');

end

%     print -dpdf fig-crispri-robustness-blue-Control.pdf
print(gcf, '-painters', '-dpdf', 'fig-crispri-robustness-blue-Control.pdf') %works perfect
