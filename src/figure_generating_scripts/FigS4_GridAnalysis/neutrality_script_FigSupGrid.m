%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
close all;clear
% dataB = readtable('.\Data\ForFigures\neutrality_position_startingAtNF_Update_20k-control.csv');
dataB = readtable('.\Data\ForFigures\neutrality_position_startingAtBlue_Update_20k-control.csv');
% dataB = readtable('.\data\ForFigures\neutrality_position_startingAtGreen_Update_20k-control.csv')
k = 2000;
Bpos1 = dataB.Neutpos1(1:k)./dataB.totalpos1(1:k);
Bpos2 = dataB.Neutpos2(1:k)./dataB.totalpos2(1:k);
Bpos3 = dataB.Neutpos3(1:k)./dataB.totalpos3(1:k);
Bpos4 = dataB.Neutpos4(1:k)./dataB.totalpos4(1:k);
Bpos5 = dataB.Neutpos5(1:k)./dataB.totalpos5(1:k);
Bpos6 = dataB.Neutpos6(1:k)./dataB.totalpos6(1:k);
Bpos7 = dataB.Neutpos7(1:k)./dataB.totalpos7(1:k);
Bpos8 = dataB.Neutpos8(1:k)./dataB.totalpos8(1:k);


v = [Bpos1;Bpos2;Bpos3;Bpos4;Bpos5;Bpos6;Bpos7;Bpos8];
c = [ones(length(Bpos1),1);ones(length(Bpos1),1)+1;ones(length(Bpos1),1)+2;
    ones(length(Bpos1),1)+3;ones(length(Bpos1),1)+4;ones(length(Bpos1),1)+5;
    ones(length(Bpos1),1)+6;ones(length(Bpos1),1)+7];
figure();
clf;
% green: [118/255 172/255 66/255]
% blue: [27/255 116/255 187/255]
% NF: [191/255 190/255 190/255]
% i have changed the x axis for violinplot command
h = violinplot(v,c ,'BoxColor',[0, 0, 0],'ViolinColor',[27/255 116/255 187/255],'ShowData',false,'ShowNotches',false,'ShowMean',true,'ViolinAlpha',0.8);

print(gcf, '-painters', '-dpdf', 'Neutrality-Blue-Control-2k.pdf') %works perfect