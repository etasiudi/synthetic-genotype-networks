%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi, J Stelling
%>  ***********************************************************************
%% Script: CRISPRi graph analysis

phenoID = {'NF','Blue','Green'};
shortID = {'N','B','G'};
cols    = [0.8*ones(1,3); 0, 0.4470, 0.7410;0.4660, 0.6740, 0.1880]; % colors

flExport = true; % flag: export figures

% stat functions
statmean = @(v) median(v);
statvar  = @(v) iqr(v);
% statmean = @(v) nanmean(v);
% statvar  = @(v) nanstd(v);

addpath .\lib\colorBrewer

if ~exist('TN','var')
    fprintf('Reading node data.\n');
%     TN = readtable('NodesTable_promeff0.csv','Delimiter',',','ReadVariableNames',true);
    TN = readtable('.\data\ForFigures\NodesTable_ControlStripe.csv','Delimiter',',','ReadVariableNames',true);

    TN.ID = uint32(TN.ID);
    TN.StripeForm = uint8(TN.StripeForm);
    v = cellfun(@str2num,TN.Name,'UniformOutput',false);
    circuitName = uint8(cell2mat(v));
    [topoName,~,circuitTopo] = unique(circuitName(:,1:6) > 0,'rows');
    circuitTopo = uint8(circuitTopo);
    TN.Name = [];
end
if ~exist('TE','var')
    fprintf('Reading edge data.\n');
%     TE = readtable('EdgesTableUNIQUE_promeff0.csv');
    TE = readtable('.\data\ForFigures\EdgesTableUNIQUE_ControlStripe.csv');

    TE.From = uint32(TE.From);
    TE.To = uint32(TE.To);
end

nT = size(topoName,1);
nP = length(phenoID);

% construct graph
if ~exist('G','var')
    G = graph(TE.From,TE.To);
end
nN = G.numnodes;


%% Compute robustness / evolvability

sF = TN.StripeForm;
nFrom = TE.From;
nTo   = TE.To;

if ~exist('robustG','var')
    fprintf('Analyzing robustness / evolvability: ');
    dG      = degree(G); % degree
    robustG = nan(nN,1); % robustness: fraction neighbors w/ same genotype
    adjG    = nan(nN,nP); % fraction of adjacent phenotypes
    evolG   = nan(nN,1); % evolvability: # new adjacent phenotypes per genotype
    evolGT  = nan(nN,1); % evolvability: # new adjacent phenotypes per genotype in same topology
    
    [nNeighborPheno,nNeighborPhenoInT] = deal(nan(nN,nP));
        
    for z = 1:nN
        idx   = neighbors(G,z);
        
        pheno     = sF(idx);
        pheno0    = sF(z);
        topo      = circuitTopo(idx);
        topo0     = circuitTopo(z);
        tidx      = topo==topo0;
        
        robustG(z) = sum(pheno==pheno0) / dG(z);
        evolG(z)   = numel(unique(pheno));
        evolGT(z)  = numel(unique(pheno(tidx)));
        
        for zp = 1:nP
            pidx = pheno==zp;
            adjG(z,zp)              = sum(pidx) / dG(z);
            nNeighborPheno(z,zp)    = sum(pidx);
            nNeighborPhenoInT(z,zp) = sum(pidx & tidx);
        end
        
        if ~mod(z,floor(nN/20))
            fprintf('.'); 
        end
    end
    fprintf('\n');
end

%% Genotype population statistics

f     = cell(nP,1);
mEvol = nan(nP,nP); % number adjacent new phenotypes
mEvolT = nan(nP,nP,nT); % number adjacent new phenotypes by topology
mAdj  = nan(nP,nP); % phenotype transition probability (median)
sAdj  = nan(nP,nP); % phenotype transition probability (interquartile range)
[mAdjT,sAdjT] = deal(nan(nP,nP,nT));

% figure(5);clf

for zf = 1:nP
    f{zf} = evolG(sF==zf);
    for zp = 1:nP % by genotype
        mEvol(zf,zp) = sum(f{zf}==zp)/numel(f{zf});
        
        v = adjG(sF==zf,zp);
        mAdj(zf,zp) = statmean(v);
        sAdj(zf,zp) = statvar(v);
%         subplot(3,3,(zf-1)*nP+zp);
%         histogram(v,'Normalization','probability');
        for zt = 1:nT
            v = adjG(sF==zf & circuitTopo==zt,zp);
            v = v(~isnan(v));
            mAdjT(zf,zp,zt) = statmean(v);
            sAdjT(zf,zp,zt) = statvar(v);
        end
    end
end

% return


%% Topology transitions

if ~exist('topoTrans','var')
    topoFrom = circuitTopo(nFrom);
    topoTo   = circuitTopo(nTo);
    
    topoTrans = nan(nT);
    fprintf('Analyzing topology transitions: ');
    for z1 = 1:nT
        idx1 = topoFrom==z1;
        for z2 = 1:nT
            idx2 = topoTo==z2;
            topoTrans(z1,z2) = sum(idx1 & idx2);
        end
        fprintf('.');
    end
    fprintf('\n');
end


%% Evolvability by topology

nGenoInT = nan(nT,1);
for zt = 1:nT
    nGenoInT(zt) = sum(circuitTopo==zt);
end

% figure(6);
% clf;
% clusterID = brewermap(3,'OrRd');
mAvgPheno = nan(nT,2);

for zv = 1:2
    if zv == 1
        nNeighP = sum(nNeighborPheno>0,2);
    else
        nNeighP = sum(nNeighborPhenoInT>0,2);
    end
    m       = nan(nT,nP);
    
    for z = 1:nT
        idx = circuitTopo==z;
        for zp = 1:nP
            m(z,zp) = sum(nNeighP(idx)==zp)/sum(idx);
        end
    end
    
%     subplot(2,1,zv);
%     for z = 1:nP
%         scatter((1:nT'),m(:,z),20,clusterID(z,:),'filled');
%         hold on;
%     end
%     xlim(.5+[0 nT]);
%     ylabel('Fraction of genotypes');
%     if zv == 2
%         xlabel('Topology');
%     end
    
    mAvgPheno(:,zv) = sum(m.*(ones(nT,1)*(1:nP)),2); % expected values for topologies    
end

% lgd = legend(num2str((1:nP)'),'Location','west');
% title(lgd,'Number phenotypes');
% legend('boxoff');

figure(7);
clf;

hold on
% connections between topologiies
[idx1,idx2] = find(topoTrans>0);
m = 40*topoTrans/max(max(topoTrans));
% m = 5*topoTrans ./ (sum(topoTrans,2)*ones(1,nT));
for z = 1:length(idx1)
    idx = [idx1(z) idx2(z)];
    h = plot(mAvgPheno(idx,1),mAvgPheno(idx,2),'k-');
    h.LineWidth = max(0.1,min(3,m(idx1(z),idx2(z))));
    h.Color = 0.75*ones(1,3);
end

nClust = 5;
clusterID = clusterdata(mAvgPheno,nClust);
c = brewermap(nClust,'Set1');
cols = c(clusterID,:);

nEdges = sum(topoName,2);
sz = 5*max(5,100*(nEdges/max(nEdges)).^2);
% sz = 10*max(5,100*(nGenoInT/max(nGenoInT)));
h = scatter(mAvgPheno(:,1),mAvgPheno(:,2),sz,cols,'filled');
h.MarkerFaceAlpha = 0.1;
h.MarkerEdgeColor = [0 0 0]; %'flat';
h.MarkerEdgeAlpha = 1;
axis equal
v = [0.8 3];
axis([v v])
hold on;
plot(v,v,'k--')
xlabel('Evolvability');
ylabel('Evolvability, fixed topology');

% topology classification and annotation
topoID = {'IFFL','MI_{BC}'};
topoFlag = [topoName(:,1) & topoName(:,2) & (topoName(:,3) | topoName(:,4)) ... % IFFL
    (topoName(:,3) & topoName(:,4))]; % mutual inhibition B-C
clusterLabel = cell(nClust,1);
for z = 1:nClust
    idx = clusterID == z;
    for zf = 1:length(topoID)
        if all(topoFlag(idx,zf))
            if isempty(clusterLabel{z})
                clusterLabel{z} = topoID{zf};
            else
                clusterLabel{z} = [clusterLabel{z} newline topoID{zf}];
            end
        end
    end
    if ~isempty(clusterLabel{z})
        pos = mean(mAvgPheno(idx,:));
        s   = strtrim(clusterLabel{z});
        text(pos(1)+.15,pos(2),s);
    end
end


%  print -dpdf fig-crispri-evolvability-Control.pdf
print(gcf, '-painters', '-dpdf', 'fig-crispri-evolvability-Control.pdf') %works perfect


return