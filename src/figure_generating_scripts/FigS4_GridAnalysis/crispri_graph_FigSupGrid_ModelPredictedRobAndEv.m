%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi, J Stelling
%>  ***********************************************************************
%% Script: CRISPRi graph analysis
clear all;close all
addpath('.\src\figure_generating_scripts\SharedScripts')
addpath('.\lib\colorBrewer')
phenoID = {'NF','Blue','Green'};
shortID = {'N','B','G'};
cols    = [0.8*ones(1,3); 0, 0.4470, 0.7410;0.4660, 0.6740, 0.1880]; % colors

flExport = true; % flag: export figures

% stat functions
statmean = @(v) median(v);
statvar  = @(v) iqr(v);
% statmean = @(v) nanmean(v);
% statvar  = @(v) nanstd(v);

addpath .\lib\colorBrewer


if ~exist('TN','var')
    fprintf('Reading node data.\n');
    TN = readtable('.\data\ForFigures\NodesTable_ControlStripe.csv','Delimiter',',','ReadVariableNames',true);

    TN.ID = uint32(TN.ID);
    TN.StripeForm = uint8(TN.StripeForm);
    v = cellfun(@str2num,TN.Name,'UniformOutput',false);
    circuitName = uint8(cell2mat(v));
    [topoName,~,circuitTopo] = unique(circuitName(:,1:6) > 0,'rows');
    circuitTopo = uint8(circuitTopo);
    TN.Name = [];
end
if ~exist('TE','var')
    fprintf('Reading edge data.\n');
    TE = readtable('.\data\ForFigures\EdgesTableUNIQUE_ControlStripe.csv');
    TE.From = uint32(TE.From);
    TE.To = uint32(TE.To);
end

nT = size(topoName,1);
nP = length(phenoID);

% construct graph
if ~exist('G','var')
    G = graph(TE.From,TE.To);
end
nN = G.numnodes;


%% Compute robustness / evolvability

sF = TN.StripeForm;
nFrom = TE.From;
nTo   = TE.To;

if ~exist('robustG','var')
    fprintf('Analyzing robustness / evolvability: ');
    dG      = degree(G); % degree
    robustG = nan(nN,1); % robustness: fraction neighbors w/ same genotype
    adjG    = nan(nN,nP); % fraction of adjacent phenotypes
    evolG   = nan(nN,1); % evolvability: # new adjacent phenotypes per genotype
    evolGT  = nan(nN,1); % evolvability: # new adjacent phenotypes per genotype in same topology
    
    [nNeighborPheno,nNeighborPhenoInT] = deal(nan(nN,nP));
        
    for z = 1:nN
        idx   = neighbors(G,z);
        
        pheno     = sF(idx);
        pheno0    = sF(z);
        topo      = circuitTopo(idx);
        topo0     = circuitTopo(z);
        tidx      = topo==topo0;
        
        robustG(z) = sum(pheno==pheno0) / dG(z);
        evolG(z)   = numel(unique(pheno));
        evolGT(z)  = numel(unique(pheno(tidx)));
        
        for zp = 1:nP
            pidx = pheno==zp;
            adjG(z,zp)              = sum(pidx) / dG(z);
            nNeighborPheno(z,zp)    = sum(pidx);
            nNeighborPhenoInT(z,zp) = sum(pidx & tidx);
        end
        
        if ~mod(z,floor(nN/20))
            fprintf('.'); 
        end
    end
    fprintf('\n');
end

%% Genotype population statistics

f     = cell(nP,1);
mEvol = nan(nP,nP); % number adjacent new phenotypes
mEvolT = nan(nP,nP,nT); % number adjacent new phenotypes by topology
mAdj  = nan(nP,nP); % phenotype transition probability (median)
sAdj  = nan(nP,nP); % phenotype transition probability (interquartile range)
[mAdjT,sAdjT] = deal(nan(nP,nP,nT));

% figure(5);clf

for zf = 1:nP
    f{zf} = evolG(sF==zf);
    for zp = 1:nP % by genotype
        mEvol(zf,zp) = sum(f{zf}==zp)/numel(f{zf});
        
        v = adjG(sF==zf,zp);
        mAdj(zf,zp) = statmean(v);
        sAdj(zf,zp) = statvar(v);
%         subplot(3,3,(zf-1)*nP+zp);
%         histogram(v,'Normalization','probability');
        for zt = 1:nT
            v = adjG(sF==zf & circuitTopo==zt,zp);
            v = v(~isnan(v));
            mAdjT(zf,zp,zt) = statmean(v);
            sAdjT(zf,zp,zt) = statvar(v);
        end
    end
end

% return

%% Genotype evolvability

figure(1);
clf;
subplot(2,2,1);
h = bar(1:nP,mEvol');
xlabel('Genotype evolvability');
ylabel('Fraction of genotypes');
for z = 1:length(h)
    h(z).FaceColor = cols(z,:);
end
if flExport
    print -dpdf fig-crispri-adjacent-phenotypes-control.pdf
end

% Phenotype transition probabilities
figure(2);
clf;
subplot(2,2,1);
h = fplottmatrix(mAdj,sAdj,phenoID,2,'4.2f');
if flExport
    print -dpdf fig-crispri-phenotype-transitions-control.pdf
end
