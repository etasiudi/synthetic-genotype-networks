%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
%% genotype network for cluster
tic
% select model topology based on total numbers of edges
NumberOfEdges = [3 4 5 6];
% select model name
ModelName = 'emptymodelOscil';
% select promoter efficiencies (they are always 2)
Nefficiencies = 5;
Kpromoters = 2;
combinationsEfficiencies = combinator(Nefficiencies,Kpromoters,'p','r');
PromEff = [1 0.75 0.5 0.25 0];


for t = 1:length(NumberOfEdges)
    
    CandidatePlaces = {'Place1','Place2','Place3','Place4','Place5','Place6'};
    AvailablePositions = 6; %this never changes
    Totaledges = NumberOfEdges(t);
    combinationTopologies = combinator(AvailablePositions,Totaledges,'c');
    
    % select type of sgRNAs
    NsgRNAs = 5;
    Kedges = Totaledges;
    combinationssgRNAs = combinator(NsgRNAs,Kedges,'p','r');
    sgRNAs = sprintfc('artsgRNAs%d',[1:2:10]);     
    
    % combine sgRNAs and prom efficiencies
    totalsgRNAEffcombinations = repelem(combinationssgRNAs,length(combinationsEfficiencies),1);
    totalEffsgRNAcombinations = repmat(combinationsEfficiencies,length(combinationssgRNAs),1);
    
    for t2 = 1:length(combinationTopologies(:,1))

        ModelTopology = CandidatePlaces(combinationTopologies(t2,:));
        folderFieldName = cell2mat(strcat((ModelTopology),'_'))
    
        for t3 = 1:length(totalsgRNAEffcombinations)
            disp((t3/(length(totalsgRNAEffcombinations)))*100)
           ParamSpecs = getParameterTable(sgRNAs(totalsgRNAEffcombinations(t3,:)),PromEff(totalEffsgRNAcombinations(t3,:)),ModelName, ModelTopology);
           TF = find(contains(ParamSpecs.names,'kbG'));
            ParamSpecs.p0(TF) = 0;
          [BinOscil(t3),QualtOscPos(t3),QualtOsc(t3),promEff(t3,:)] = OscilSimulationNoData(ModelName,ParamSpecs,2.031,false);

        end
        
        GM.(folderFieldName).BinOscil = BinOscil';
        GM.(folderFieldName).QualtOscPos = QualtOscPos';
        GM.(folderFieldName).QualtOsc = QualtOsc';
        GM.(folderFieldName).proEf = promEff;
        GM.(folderFieldName).sgRNAEffcombinations = totalsgRNAEffcombinations;
        GM.(folderFieldName).EffsgRNAcombinations = totalEffsgRNAcombinations;
       end
end
toc