%>  *********************************************************************** 
%>   Copyright � 2021-2023 ETH Zurich, J S Moreno, E Tasiudi, H
%    Kusumawardhani, J Stelling, Y Shaerli
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the GNU General Public License v3. 
%>   which accompanies this distribution
%> % Author: E Tasiudi
%>  ***********************************************************************
function [paraoptlist, name, cost] = mainOptimization (varargin)
%% perform optimization for parameter estimation using WLS
% data to use 2-node, 1.4, 2cnf1, 3.2, 4.1
% these are models: 2node, node3_caseA,node3_caseB, node3_caseD,
% node3_caseF, respectively
if nargin == 1
    maxeval = varargin{1};
else
    maxeval = 200000;
end


% node2 = [13321.788,13.322,6.661,3.33,1.665,0.833,0.416,0.208,0]; %
% node3 = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];




%% perform simultaneous optimization for all 
thisModel = {'node2','node3_caseA','node3_caseB','node3_caseD','node3_caseF'};

paramSpecs = readtable('./src/parameters/paramSpecs_for_optimization_Updated.txt');

thisData = {'./data/2node/unormalized_data_2node.mat';...
    './data/3node/1.4/unormalized_data_14.mat';...
    './Data/3node/2c-NF.1/unormalized_data_2cNF1.mat';...
    './Data/3node/3.2/unormalized_data_32.mat';...
    './Data/3node/4.1/unormalized_data_41.mat'};
GrowthParam = {'./src/growth_related_opt/2_node/growth_param_2node.mat';...
    './src/growth_related_opt/3_node/growth_param_c14.mat';...
    './src/growth_related_opt/3_node/growth_param_2cnf1.mat';...
    './src/growth_related_opt/3_node/growth_param_32.mat';...
    './src/growth_related_opt/3_node/growth_param_41.mat'};

% the sgRNAs used for inhibitions- sequence is important!
node3Seq = {[],...
    {'sg1t4','sg2','sg1'},...
    {'sg1','sg2','sg1','sg4'},...
    {'sg1','sg2','sg1','sg4','sg4'},...
    {'sg1','sg2','sg1','sg4','sg4','sg3'}};

timepointExp = {[10:10:900]',[10:10:900]',...
    [10:10:900]',[10:10:900]',[10:10:900]'};

araTotal3node = [13321.788;4442.816;1478.718;493.572;164.524;54.82;18.25;6.088;2.031;0.679;0.226;0.075;0.025;0.008;0.003;0];
araTotal2node = [13321.788,13.322,6.661,3.33,1.665,0.833,0.416,0.208,0];
arabinoseExp = {araTotal2node,araTotal3node,...
    araTotal3node,araTotal3node,araTotal3node};

[~, ~, ~, paramValueNotFixed, ID] = getFixedAndNonFixed(paramSpecs);
FlPlot = true;
% the sgRNAs in the 2 node model to individually optimize
sgRNACasesNames = {{'sg1','sg2','sg3','sg4','sg4t4','sg1t4'}...
    ,[],[],[],[]};


jointOpt = true;
setObjectiveSimulation(paramSpecs.p0(ID), paramSpecs, FlPlot, arabinoseExp, timepointExp...
,thisModel, thisData,GrowthParam,sgRNACasesNames,jointOpt,node3Seq);



%% run meigo
FlPlot = false;
npoints = 200;
[paraoptlist, name, cost] = setMeigo(paramValueNotFixed,paramSpecs, FlPlot, arabinoseExp, timepointExp,...
    thisModel, thisData, GrowthParam, npoints, maxeval,sgRNACasesNames,jointOpt,node3Seq);


save('./results/results_estimation/Result_params_Optimization_example.mat','paraoptlist', 'name', 'cost','paramSpecs');
end





