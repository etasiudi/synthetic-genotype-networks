
# Synthetic Genotype Networks

## Information

This repository contains data and source files associated with the following manuscript: <br>
J Santos-Moreno*, E Tasiudi*, H Kusumawardhani,  J Stelling# & Y Schaerli# __Robustness and innovation in synthetic genotype networks__

*These authors contributed equally

#Corresponding authors:
J Stelling <joerg.stelling@bsse.ethz.ch>
Y Schaerli <yolanda.schaerli@unil.ch>

## Content

* `data`: Raw data and data used for figures in the paper.
* `lib`: External packages used in the paper used for analysis (have to be installed if run for first time).
* `results`: Results generated in this paper
	* `results_estimation`: Results from optimization process for parameter estimation.
	* `results_genotype`: Results from simulations of the full genotype network.
* `src`: Source code generated for this paper.
	* `figure_generating_scripts`: reproduce the figures in the paper.
	* `growth_related_opt`: growth related parameters.
	* `main_scripts_full_genotype_network`: reproduce full genotype network.
	* `main_scripts_optimization`: reproduce parameter estimation process.
	* `models`: All models neseccary and mentioned in the paper.
	* `common_scripts`: Utility functions.
	* `parameters`: Parameters for parameter estimation and for full genotype network.
* Analysis scripts in MATLAB.

## Description

Initially to be able to run all three main MATLAB scripts, initialize IQMtools as suggested in its own documentation
(this is in folder lib).

To generate model-related figures from main text and supplementary material run scripts:
1) `run_for_main_text_figures` and 2) `run_for_supplementary_text_figures`. However, these two scripts
do not run the Rstudio generated figure Fig 4A and SF 4A. For this please go to individual figure related
folders and run scripts in folders 'scripts_for_R_tree_generation' and 'Rscript_tree_grid' respectively.

To perform optimization for parameter estimation and generate the full genotype network run the
script `run_for_optimization_AND_full_genotype_network`. Attention these scripts are computationally demanding
and suggested to be ran on a server.

## Requirements

* MATLAB version 2018b
* Rstudio 2021.09.2 Build 382
* Library dependencies needed in lib folder

## Author

Eve Tasiudi <etasiudi@bsse.ethz.ch>

If you encounter a problem, please contact the corresponding author.

## Licence
GNU General Public License v3

